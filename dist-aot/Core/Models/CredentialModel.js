export class CredentialModel {
    constructor(username, password, rememberMe = false) {
        this.username = username;
        this.password = password;
        this.rememberMe = rememberMe;
    }
}
//# sourceMappingURL=CredentialModel.js.map