export class UserInfoModel {
    constructor(FullName = "", FirstName = "", LastName = "", EMail = "", FullNameReverse = "") {
        this.FullName = FullName;
        this.FirstName = FirstName;
        this.LastName = LastName;
        this.EMail = EMail;
        this.FullNameReverse = FullNameReverse;
    }
}
//# sourceMappingURL=UserInfoModel.js.map