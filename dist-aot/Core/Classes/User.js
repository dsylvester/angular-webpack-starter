import { UserDetail } from "./UserDetail";
export class User {
    constructor(ID = 0, FirstName = "", LastName = "", Phone = "", Roles = new Array(), Details = new UserDetail(), FullName = "") {
        this.ID = ID;
        this.FirstName = FirstName;
        this.LastName = LastName;
        this.Phone = Phone;
        this.Roles = Roles;
        this.Details = Details;
        this.FullName = FullName;
        this.FullName = `${this.FirstName} ${this.LastName}`;
    }
}
//# sourceMappingURL=User.js.map