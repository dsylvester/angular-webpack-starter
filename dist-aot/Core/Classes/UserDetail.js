export class UserDetail {
    constructor(UserID = 0, ForcePasswordChange = false, AccountLocked = false) {
        this.UserID = UserID;
        this.ForcePasswordChange = ForcePasswordChange;
        this.AccountLocked = AccountLocked;
    }
}
//# sourceMappingURL=UserDetail.js.map