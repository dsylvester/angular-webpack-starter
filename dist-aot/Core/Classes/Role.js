export class Role {
    constructor(ID = 0, SecurityLevel = 0, Name = "", Description = "") {
        this.ID = ID;
        this.SecurityLevel = SecurityLevel;
        this.Name = Name;
        this.Description = Description;
    }
}
//# sourceMappingURL=Role.js.map