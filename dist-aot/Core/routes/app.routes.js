import { RouterModule } from "@angular/router";
import { NavigationRoutes } from "./NavigationRoutes";
const routes = [];
routes.push(...NavigationRoutes);
export const appRoutingProviders = [];
export const routing = RouterModule.forRoot(routes);
//# sourceMappingURL=app.routes.js.map