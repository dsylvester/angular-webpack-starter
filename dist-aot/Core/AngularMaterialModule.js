var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from "@angular/core";
import { PortalModule } from "@angular/cdk/portal";
import { MatButtonModule, MatCardModule, MatChipsModule, MatDialogModule, MatIconModule, MatInputModule, MatMenuModule, MatProgressBarModule, MatSelectModule, MatToolbarModule, MatGridListModule, MatTabsModule, MatRadioModule, MatTableModule, MatTooltipModule, MatCheckboxModule, MatProgressSpinnerModule, MatSnackBarModule } from "@angular/material";
import { CdkTableModule } from '@angular/cdk/table';
let AngularMaterialModule = class AngularMaterialModule {
};
AngularMaterialModule = __decorate([
    NgModule({
        exports: [
            CdkTableModule,
            MatButtonModule,
            MatCardModule,
            MatChipsModule,
            MatDialogModule,
            MatIconModule,
            MatInputModule,
            MatMenuModule,
            MatProgressBarModule,
            MatSelectModule,
            MatToolbarModule,
            MatGridListModule,
            MatTabsModule,
            MatRadioModule,
            MatTableModule,
            MatTooltipModule,
            MatCheckboxModule,
            PortalModule,
            MatProgressSpinnerModule,
            MatProgressBarModule,
            MatSnackBarModule
        ],
        entryComponents: []
    })
], AngularMaterialModule);
export { AngularMaterialModule };
//# sourceMappingURL=AngularMaterialModule.js.map