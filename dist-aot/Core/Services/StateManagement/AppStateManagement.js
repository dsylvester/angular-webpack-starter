var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { AppGlobal } from "./Classes/index";
import { AuthEvents } from "./Reducers/AuthEvents";
import { UserInfoModel } from "../../Models/index";
import { WebTokenHelper } from "../../Helpers/index";
let AppStateManagement = class AppStateManagement {
    constructor(router, authEvents, webTokenHelper) {
        this.router = router;
        this.authEvents = authEvents;
        this.webTokenHelper = webTokenHelper;
        this.init();
    }
    init() {
        this.app = new AppGlobal();
        this.subscriptions();
    }
    subscriptions() {
        this.subscribeToAuthEvents();
    }
    subscribeToAuthEvents() {
        this.authSubscription = this.authEvents.authEvent.subscribe(x => {
            if (x) {
                this.setUserAsLoggedIn();
            }
            else {
                this.setUserAsLoggedOut();
            }
        });
    }
    setUserAsLoggedOut() {
        this.app = new AppGlobal();
        this.router.navigateByUrl(`/login`);
    }
    setUserAsLoggedIn() {
        this.app.LoggedIn = true;
        const tokenObj = this.webTokenHelper.mapFromWebToken(this.app.Token);
        this.app.UserInfo = new UserInfoModel(`${tokenObj.FirstName} ${tokenObj.LastName}`, tokenObj.FirstName, tokenObj.LastName, "", `${tokenObj.LastName}, ${tokenObj.FirstName} `);
        this.router.navigateByUrl(`/dashboard`);
    }
    unSubscribe() {
        this.authSubscription.unsubscribe();
    }
};
AppStateManagement = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [Router,
        AuthEvents,
        WebTokenHelper])
], AppStateManagement);
export { AppStateManagement };
//# sourceMappingURL=AppStateManagement.js.map