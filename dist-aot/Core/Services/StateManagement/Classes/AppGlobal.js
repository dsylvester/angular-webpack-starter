export class AppGlobal {
    constructor() {
        this.loggedIn = false;
        this.UserInfo = null;
        this.Token = null;
        this.Roles = null;
        this.LoadingIndicator = false;
    }
    get LoggedIn() {
        return this.loggedIn;
    }
    set LoggedIn(val) {
        this.loggedIn = val;
    }
}
//# sourceMappingURL=AppGlobal.js.map