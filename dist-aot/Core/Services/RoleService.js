var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { map } from "rxjs/operators";
import { EndPoints } from "./EndPoints";
import { RoleHelper } from "../Helpers/index";
import { Role } from "../Classes/index";
let RoleService = class RoleService {
    constructor(http, roleHelper) {
        this.http = http;
        this.roleHelper = roleHelper;
        this.Init();
    }
    Init() {
        this.createHeaders();
    }
    createHeaders() {
        this.headers = new HttpHeaders({
            "content-type": "application/json",
            "Accept": "application/json",
        });
    }
    Get(id = null) {
        if (id === null) {
            return this.http.get(EndPoints.RoleURL, { headers: this.headers })
                .pipe(map(x => {
                return this.roleHelper.mapToRoles(x);
            }));
        }
        else {
            return this.http.get(EndPoints.RoleURL)
                .pipe(map(x => {
                return this.roleHelper.mapToRole(x);
            }));
        }
    }
    Post() {
        return new Role();
    }
    Put() {
        return new Role();
    }
    Delete() {
    }
};
RoleService = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [HttpClient,
        RoleHelper])
], RoleService);
export { RoleService };
//# sourceMappingURL=RoleService.js.map