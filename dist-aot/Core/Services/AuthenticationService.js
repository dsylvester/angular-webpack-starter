var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { EndPoints } from "./EndPoints";
import { Observable } from "rxjs";
import { map, catchError } from "rxjs/operators";
let AuthenticationService = class AuthenticationService {
    constructor(http) {
        this.http = http;
        this.Init();
    }
    Init() {
        this.createHeaders();
    }
    createHeaders() {
        if (EndPoints.Environment === "dev") {
            this.headers = new HttpHeaders({
                "content-type": "application/json",
                "Accept": "application/json",
                "Authorization": "toughsecurity"
            });
        }
        else {
            this.headers = new HttpHeaders({
                "content-type": "application/json",
                "Accept": "application/json"
            });
        }
    }
    CheckLogin(data) {
        return this.http.post(EndPoints.AuthenticationURL, data, { headers: this.headers })
            .pipe(map(x => {
            return x;
        }))
            .pipe(catchError(x => {
            console.log(JSON.stringify(x, null, 5));
            return Observable.throw(x);
        }));
    }
};
AuthenticationService = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [HttpClient])
], AuthenticationService);
export { AuthenticationService };
//# sourceMappingURL=AuthenticationService.js.map