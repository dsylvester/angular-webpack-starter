const Base = "http://localhost:6901";
export const EndPoints = {
    Environment: "dev",
    AuthenticationURL: `${Base}/api/authentication`,
    RoleURL: `${Base}/api/secured/role`,
};
//# sourceMappingURL=EndPoints.js.map