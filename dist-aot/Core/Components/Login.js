var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { AuthenticationService, AppStateManagement } from "../Services/index";
import { CredentialModel } from "../Models/index";
import { MatSnackBar } from '@angular/material';
import { WebTokenHelper } from "../Helpers/index";
import "../Html/Login.less";
import "../Html/snack-davis.less";
let Login = class Login {
    constructor(authService, router, snackBar, stateManagement, webTokenHelper) {
        this.authService = authService;
        this.router = router;
        this.snackBar = snackBar;
        this.stateManagement = stateManagement;
        this.webTokenHelper = webTokenHelper;
        this.model = {
            username: '',
            password: '',
            rememberMe: false
        };
        this.showLoginPanel = true;
        this.Init();
    }
    Init() {
        console.log(`STarting Login`);
    }
    ngOnInit() {
    }
    checkLogin() {
        if (!this.validateLogin()) {
            return;
        }
        let model = new CredentialModel(this.model.username, this.model.password);
        this.stateManagement.app.LoadingIndicator = true;
        this.authService.CheckLogin(model)
            .subscribe(x => {
            console.log(x);
            this.stateManagement.app.Token = x;
            this.stateManagement.authEvents.onLogin();
            console.log(this.stateManagement.app);
        }, y => {
            this.snackBar.open(``, `Incorrect Username / Password combination`, {
                duration: 100000,
                verticalPosition: "top",
                horizontalPosition: "center",
                panelClass: "snack-davis"
            });
            this.model.password = "";
            this.model.username = "";
            console.log(JSON.stringify(y, null, 4));
        }, () => {
            this.stateManagement.app.LoadingIndicator = false;
        });
    }
    forgotPassword() {
        this.router.navigateByUrl(`/recover-password`);
    }
    resetPassword() {
        this.router.navigateByUrl(`/recover-password`);
    }
    navigateToRecoverPassword() {
        console.log(`Navigate to Recover Password`);
        this.toggleShowLoginPanel();
    }
    toggleShowLoginPanel() {
        this.showLoginPanel = (this.showLoginPanel) ? false : true;
    }
    cancel() {
        this.toggleShowLoginPanel();
    }
    validateLogin() {
        const systemAccount = [
            "davis@sylvesterllc.com",
            "admin@TSG.Core.com",
            "system@TSG.Core.com",
            "developers@sylvento.com"
        ];
        const isSystemAccount = systemAccount.some(x => {
            return this.model.username.toLocaleLowerCase() === x.toLocaleLowerCase();
        });
        if (isSystemAccount) {
            return true;
        }
        if (this.model.username.length >= 7 && this.model.password.length >= 5) {
            return true;
        }
        if (this.model.username.length < 7) {
            alert("Your E-mail address must have at least 8 characters");
        }
        if (this.model.password.length < 4) {
            alert("Your Password must have at least 8 characters");
        }
        return false;
    }
};
Login = __decorate([
    Component({
        selector: "tsg-login-panel",
        template: "<div class=\"login-background\" simple-snack-bar>\r\n  <div class=\"login-container\">\r\n    <div class=\"login-container-section-wrapper\">\r\n      <div class=\"sign-up\">\r\n        <div class=\"tsg-form-control\">\r\n          <input type=\"text\" placeholder=\"email\" autocomplete=\"off\" [(ngModel)]=\"model.username\" />\r\n          <i class=\"fal fa-user\"></i>\r\n        </div>\r\n\r\n        <div class=\"tsg-form-control\">\r\n          <input type=\"password\" placeholder=\"password\" [(ngModel)]=\"model.password\" />\r\n          <i class=\"far fa-lock-open-alt\"></i>\r\n        </div>\r\n\r\n        <button (click)=\"checkLogin()\">Login</button>\r\n\r\n        <p class=\"text-center\" (click)=\"forgotPassword()\">Forgot Password / Username</p>\r\n        <p class=\"text-center\" (click)=\"resetPassword()\">Reset Password / Username</p>\r\n      </div>\r\n\r\n      <div class=\"container-spacer\"></div>\r\n      <div class=\"new-account\" style=\"padding: 2%;\">\r\n          <p style=\"text-align: left; font-weight: 900;\r\n          font-size: 32px;text-transform: none;font-stretch:300%;\">New <span\r\n            style=\"color: #6334a1;\">User</span></p>\r\n          <p style=\"text-align: left; text-transform: none;font-size: 18px;\">Setup an account</p>\r\n          <p style=\"text-transform: uppercase; font-family: 'Open Sans'; font-size: 225%;\">OR</p>\r\n          <p style=\"text-align: left; text-transform: none;font-size: 18px;\">\r\n            Sign Up today to begin using our state\r\n            of the art system!</p>\r\n\r\n          <button style=\"width: 100%;\">Create Account</button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n",
        animations: []
    }),
    __metadata("design:paramtypes", [AuthenticationService,
        Router,
        MatSnackBar,
        AppStateManagement,
        WebTokenHelper])
], Login);
export { Login };
//# sourceMappingURL=Login.js.map