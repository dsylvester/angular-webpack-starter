import * as i0 from "@angular/core";
import * as i1 from "@angular/forms";
import * as i2 from "./Login";
import * as i3 from "../Services/AuthenticationService";
import * as i4 from "@angular/router";
import * as i5 from "@angular/material/snack-bar";
import * as i6 from "../Services/StateManagement/AppStateManagement";
import * as i7 from "../Helpers/WebTokenHelper";
var styles_Login = [];
var RenderType_Login = i0.ɵcrt({ encapsulation: 2, styles: styles_Login, data: {} });
export { RenderType_Login as RenderType_Login };
export function View_Login_0(_l) { return i0.ɵvid(0, [(_l()(), i0.ɵeld(0, 0, null, null, 65, "div", [["class", "login-background"], ["simple-snack-bar", ""]], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ["\n  "])), (_l()(), i0.ɵeld(2, 0, null, null, 62, "div", [["class", "login-container"]], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ["\n    "])), (_l()(), i0.ɵeld(4, 0, null, null, 59, "div", [["class", "login-container-section-wrapper"]], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ["\n      "])), (_l()(), i0.ɵeld(6, 0, null, null, 34, "div", [["class", "sign-up"]], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ["\n        "])), (_l()(), i0.ɵeld(8, 0, null, null, 10, "div", [["class", "tsg-form-control"]], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ["\n          "])), (_l()(), i0.ɵeld(10, 0, null, null, 5, "input", [["autocomplete", "off"], ["placeholder", "email"], ["type", "text"]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "ngModelChange"], [null, "input"], [null, "blur"], [null, "compositionstart"], [null, "compositionend"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("input" === en)) {
        var pd_0 = (i0.ɵnov(_v, 11)._handleInput($event.target.value) !== false);
        ad = (pd_0 && ad);
    } if (("blur" === en)) {
        var pd_1 = (i0.ɵnov(_v, 11).onTouched() !== false);
        ad = (pd_1 && ad);
    } if (("compositionstart" === en)) {
        var pd_2 = (i0.ɵnov(_v, 11)._compositionStart() !== false);
        ad = (pd_2 && ad);
    } if (("compositionend" === en)) {
        var pd_3 = (i0.ɵnov(_v, 11)._compositionEnd($event.target.value) !== false);
        ad = (pd_3 && ad);
    } if (("ngModelChange" === en)) {
        var pd_4 = ((_co.model.username = $event) !== false);
        ad = (pd_4 && ad);
    } return ad; }, null, null)), i0.ɵdid(11, 16384, null, 0, i1.DefaultValueAccessor, [i0.Renderer2, i0.ElementRef, [2, i1.COMPOSITION_BUFFER_MODE]], null, null), i0.ɵprd(1024, null, i1.NG_VALUE_ACCESSOR, function (p0_0) { return [p0_0]; }, [i1.DefaultValueAccessor]), i0.ɵdid(13, 671744, null, 0, i1.NgModel, [[8, null], [8, null], [8, null], [6, i1.NG_VALUE_ACCESSOR]], { model: [0, "model"] }, { update: "ngModelChange" }), i0.ɵprd(2048, null, i1.NgControl, null, [i1.NgModel]), i0.ɵdid(15, 16384, null, 0, i1.NgControlStatus, [[4, i1.NgControl]], null, null), (_l()(), i0.ɵted(-1, null, ["\n          "])), (_l()(), i0.ɵeld(17, 0, null, null, 0, "i", [["class", "fal fa-user"]], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ["\n        "])), (_l()(), i0.ɵted(-1, null, ["\n\n        "])), (_l()(), i0.ɵeld(20, 0, null, null, 10, "div", [["class", "tsg-form-control"]], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ["\n          "])), (_l()(), i0.ɵeld(22, 0, null, null, 5, "input", [["placeholder", "password"], ["type", "password"]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "ngModelChange"], [null, "input"], [null, "blur"], [null, "compositionstart"], [null, "compositionend"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("input" === en)) {
        var pd_0 = (i0.ɵnov(_v, 23)._handleInput($event.target.value) !== false);
        ad = (pd_0 && ad);
    } if (("blur" === en)) {
        var pd_1 = (i0.ɵnov(_v, 23).onTouched() !== false);
        ad = (pd_1 && ad);
    } if (("compositionstart" === en)) {
        var pd_2 = (i0.ɵnov(_v, 23)._compositionStart() !== false);
        ad = (pd_2 && ad);
    } if (("compositionend" === en)) {
        var pd_3 = (i0.ɵnov(_v, 23)._compositionEnd($event.target.value) !== false);
        ad = (pd_3 && ad);
    } if (("ngModelChange" === en)) {
        var pd_4 = ((_co.model.password = $event) !== false);
        ad = (pd_4 && ad);
    } return ad; }, null, null)), i0.ɵdid(23, 16384, null, 0, i1.DefaultValueAccessor, [i0.Renderer2, i0.ElementRef, [2, i1.COMPOSITION_BUFFER_MODE]], null, null), i0.ɵprd(1024, null, i1.NG_VALUE_ACCESSOR, function (p0_0) { return [p0_0]; }, [i1.DefaultValueAccessor]), i0.ɵdid(25, 671744, null, 0, i1.NgModel, [[8, null], [8, null], [8, null], [6, i1.NG_VALUE_ACCESSOR]], { model: [0, "model"] }, { update: "ngModelChange" }), i0.ɵprd(2048, null, i1.NgControl, null, [i1.NgModel]), i0.ɵdid(27, 16384, null, 0, i1.NgControlStatus, [[4, i1.NgControl]], null, null), (_l()(), i0.ɵted(-1, null, ["\n          "])), (_l()(), i0.ɵeld(29, 0, null, null, 0, "i", [["class", "far fa-lock-open-alt"]], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ["\n        "])), (_l()(), i0.ɵted(-1, null, ["\n\n        "])), (_l()(), i0.ɵeld(32, 0, null, null, 1, "button", [], null, [[null, "click"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("click" === en)) {
        var pd_0 = (_co.checkLogin() !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), (_l()(), i0.ɵted(-1, null, ["Login"])), (_l()(), i0.ɵted(-1, null, ["\n\n        "])), (_l()(), i0.ɵeld(35, 0, null, null, 1, "p", [["class", "text-center"]], null, [[null, "click"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("click" === en)) {
        var pd_0 = (_co.forgotPassword() !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), (_l()(), i0.ɵted(-1, null, ["Forgot Password / Username"])), (_l()(), i0.ɵted(-1, null, ["\n        "])), (_l()(), i0.ɵeld(38, 0, null, null, 1, "p", [["class", "text-center"]], null, [[null, "click"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("click" === en)) {
        var pd_0 = (_co.resetPassword() !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), (_l()(), i0.ɵted(-1, null, ["Reset Password / Username"])), (_l()(), i0.ɵted(-1, null, ["\n      "])), (_l()(), i0.ɵted(-1, null, ["\n\n      "])), (_l()(), i0.ɵeld(42, 0, null, null, 0, "div", [["class", "container-spacer"]], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ["\n      "])), (_l()(), i0.ɵeld(44, 0, null, null, 18, "div", [["class", "new-account"], ["style", "padding: 2%;"]], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ["\n          "])), (_l()(), i0.ɵeld(46, 0, null, null, 3, "p", [["style", "text-align: left; font-weight: 900;\n          font-size: 32px;text-transform: none;font-stretch:300%;"]], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ["New "])), (_l()(), i0.ɵeld(48, 0, null, null, 1, "span", [["style", "color: #6334a1;"]], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ["User"])), (_l()(), i0.ɵted(-1, null, ["\n          "])), (_l()(), i0.ɵeld(51, 0, null, null, 1, "p", [["style", "text-align: left; text-transform: none;font-size: 18px;"]], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ["Setup an account"])), (_l()(), i0.ɵted(-1, null, ["\n          "])), (_l()(), i0.ɵeld(54, 0, null, null, 1, "p", [["style", "text-transform: uppercase; font-family: 'Open Sans'; font-size: 225%;"]], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ["OR"])), (_l()(), i0.ɵted(-1, null, ["\n          "])), (_l()(), i0.ɵeld(57, 0, null, null, 1, "p", [["style", "text-align: left; text-transform: none;font-size: 18px;"]], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ["\n            Sign Up today to begin using our state\n            of the art system!"])), (_l()(), i0.ɵted(-1, null, ["\n\n          "])), (_l()(), i0.ɵeld(60, 0, null, null, 1, "button", [["style", "width: 100%;"]], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ["Create Account"])), (_l()(), i0.ɵted(-1, null, ["\n      "])), (_l()(), i0.ɵted(-1, null, ["\n    "])), (_l()(), i0.ɵted(-1, null, ["\n  "])), (_l()(), i0.ɵted(-1, null, ["\n"])), (_l()(), i0.ɵted(-1, null, ["\n"]))], function (_ck, _v) { var _co = _v.component; var currVal_7 = _co.model.username; _ck(_v, 13, 0, currVal_7); var currVal_15 = _co.model.password; _ck(_v, 25, 0, currVal_15); }, function (_ck, _v) { var currVal_0 = i0.ɵnov(_v, 15).ngClassUntouched; var currVal_1 = i0.ɵnov(_v, 15).ngClassTouched; var currVal_2 = i0.ɵnov(_v, 15).ngClassPristine; var currVal_3 = i0.ɵnov(_v, 15).ngClassDirty; var currVal_4 = i0.ɵnov(_v, 15).ngClassValid; var currVal_5 = i0.ɵnov(_v, 15).ngClassInvalid; var currVal_6 = i0.ɵnov(_v, 15).ngClassPending; _ck(_v, 10, 0, currVal_0, currVal_1, currVal_2, currVal_3, currVal_4, currVal_5, currVal_6); var currVal_8 = i0.ɵnov(_v, 27).ngClassUntouched; var currVal_9 = i0.ɵnov(_v, 27).ngClassTouched; var currVal_10 = i0.ɵnov(_v, 27).ngClassPristine; var currVal_11 = i0.ɵnov(_v, 27).ngClassDirty; var currVal_12 = i0.ɵnov(_v, 27).ngClassValid; var currVal_13 = i0.ɵnov(_v, 27).ngClassInvalid; var currVal_14 = i0.ɵnov(_v, 27).ngClassPending; _ck(_v, 22, 0, currVal_8, currVal_9, currVal_10, currVal_11, currVal_12, currVal_13, currVal_14); }); }
export function View_Login_Host_0(_l) { return i0.ɵvid(0, [(_l()(), i0.ɵeld(0, 0, null, null, 1, "tsg-login-panel", [], null, null, null, View_Login_0, RenderType_Login)), i0.ɵdid(1, 114688, null, 0, i2.Login, [i3.AuthenticationService, i4.Router, i5.MatSnackBar, i6.AppStateManagement, i7.WebTokenHelper], null, null)], function (_ck, _v) { _ck(_v, 1, 0); }, null); }
var LoginNgFactory = i0.ɵccf("tsg-login-panel", i2.Login, View_Login_Host_0, {}, {}, []);
export { LoginNgFactory as LoginNgFactory };
//# sourceMappingURL=Login.ngfactory.js.map