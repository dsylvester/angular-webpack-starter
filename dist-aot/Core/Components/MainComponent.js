var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from "@angular/core";
import { RouterModule } from "@angular/router";
import { RoleService, AppStateManagement } from "../Services/index";
import "@angular/material/prebuilt-themes/indigo-pink.css";
import "@fortawesome/fontawesome-pro/css/all.min.css";
let MainComponent = class MainComponent {
    constructor(roleService, stateManagement) {
        this.roleService = roleService;
        this.stateManagement = stateManagement;
        this.Title = "Main Component Title";
        console.log("Starting Main Component");
    }
    ngOnInit() {
        this.getRolesFromApi();
    }
    ngAfterViewInit() {
    }
    getRolesFromApi() {
        this.roleService.Get().subscribe(x => {
            this.stateManagement.app.Roles = x;
            console.log(this.stateManagement.app.Roles);
        }, x => {
            console.error(x);
        }, () => {
        });
    }
};
MainComponent = __decorate([
    Component({
        selector: "main-app",
        moduleId: module.id.toString(),
        template: "<div id=\"main-app-container\" class=\"sf-container-stack\">\r\n    <mat-progress-bar [style.display]=\"stateManagement.app.LoadingIndicator ? 'block' : 'none'\" mode=\"indeterminate\">\r\n    </mat-progress-bar>\r\n\r\n    <router-outlet>\r\n\r\n    </router-outlet>\r\n\r\n</div>",
        providers: [RouterModule],
        styles: [":host {\r\n  width: 100vw;\r\n  max-width: 100%;\r\n  min-height: 100vh;\r\n  display: -webkit-box;\r\n  display: -ms-flexbox;\r\n  display: flex;\r\n  -webkit-box-orient: vertical;\r\n  -webkit-box-direction: normal;\r\n      -ms-flex-direction: column;\r\n          flex-direction: column;\r\n  margin: 0;\r\n  -webkit-box-flex: 1;\r\n      -ms-flex-positive: 1;\r\n          flex-grow: 1;\r\n          \r\n}\r\n\r\n/*# sourceMappingURL=styles.css.map */\r\n"]
    }),
    __metadata("design:paramtypes", [RoleService,
        AppStateManagement])
], MainComponent);
export { MainComponent };
//# sourceMappingURL=MainComponent.js.map