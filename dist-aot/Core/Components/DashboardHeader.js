var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from "@angular/core";
import { AppStateManagement } from "../Services/index";
import "../html/DashboardHeader.less";
let DashboardHeader = class DashboardHeader {
    constructor(stateManagement) {
        this.stateManagement = stateManagement;
        this.Init();
    }
    Init() {
    }
    logout() {
        this.stateManagement.authEvents.onLogout();
    }
    ngOnInit() {
    }
};
DashboardHeader = __decorate([
    Component({
        selector: "tsg-dashboard-header",
        template: "<div id=\"dashboard-header-container\">\r\n        <ul>\r\n                <li><i class=\"fas fa-home\"></i> Home</li>\r\n                <li><i class=\"far fa-cubes\"></i>Projects</li>\r\n        </ul>\r\n\r\n        <div>\r\n                <span class=\"logged-in-user\" *ngIf=\"stateManagement?.app?.UserInfo?.FullName\">\r\n                        <i class=\"fas fa-user-cog\"></i>\r\n                        {{ stateManagement?.app?.UserInfo?.FullName }}\r\n                </span>\r\n        </div>\r\n\r\n        <div>\r\n                <span \r\n                        class=\"logged-in-user\" *ngIf=\"stateManagement?.app?.LoggedIn\"\r\n                        (click)=\"logout()\">\r\n                        <i class=\"fas fa-sign-out-alt\"></i>Sign Off\r\n                </span>\r\n        </div>\r\n\r\n\r\n        \r\n</div>",
        animations: []
    }),
    __metadata("design:paramtypes", [AppStateManagement])
], DashboardHeader);
export { DashboardHeader };
//# sourceMappingURL=DashboardHeader.js.map