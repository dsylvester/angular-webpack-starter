import * as i0 from "../html/styles.css.shim.ngstyle";
import * as i1 from "@angular/core";
import * as i2 from "../../../../../node_modules/@angular/material/progress-bar/typings/index.ngfactory";
import * as i3 from "@angular/material/progress-bar";
import * as i4 from "@angular/platform-browser/animations";
import * as i5 from "@angular/router";
import * as i6 from "./MainComponent";
import * as i7 from "../Services/RoleService";
import * as i8 from "../Services/StateManagement/AppStateManagement";
var styles_MainComponent = [i0.styles];
var RenderType_MainComponent = i1.ɵcrt({ encapsulation: 0, styles: styles_MainComponent, data: {} });
export { RenderType_MainComponent as RenderType_MainComponent };
export function View_MainComponent_0(_l) { return i1.ɵvid(0, [(_l()(), i1.ɵeld(0, 0, null, null, 9, "div", [["class", "sf-container-stack"], ["id", "main-app-container"]], null, null, null, null, null)), (_l()(), i1.ɵted(-1, null, ["\n    "])), (_l()(), i1.ɵeld(2, 0, null, null, 2, "mat-progress-bar", [["aria-valuemax", "100"], ["aria-valuemin", "0"], ["class", "mat-progress-bar"], ["mode", "indeterminate"], ["role", "progressbar"]], [[4, "display", null], [1, "aria-valuenow", 0], [1, "mode", 0], [2, "_mat-animation-noopable", null]], null, null, i2.View_MatProgressBar_0, i2.RenderType_MatProgressBar)), i1.ɵdid(3, 4374528, null, 0, i3.MatProgressBar, [i1.ElementRef, i1.NgZone, [2, i4.ANIMATION_MODULE_TYPE], [2, i3.MAT_PROGRESS_BAR_LOCATION]], { mode: [0, "mode"] }, null), (_l()(), i1.ɵted(-1, null, ["\n    "])), (_l()(), i1.ɵted(-1, null, ["\n\n    "])), (_l()(), i1.ɵeld(6, 16777216, null, null, 2, "router-outlet", [], null, null, null, null, null)), i1.ɵdid(7, 212992, null, 0, i5.RouterOutlet, [i5.ChildrenOutletContexts, i1.ViewContainerRef, i1.ComponentFactoryResolver, [8, null], i1.ChangeDetectorRef], null, null), (_l()(), i1.ɵted(-1, null, ["\n\n    "])), (_l()(), i1.ɵted(-1, null, ["\n\n"]))], function (_ck, _v) { var currVal_4 = "indeterminate"; _ck(_v, 3, 0, currVal_4); _ck(_v, 7, 0); }, function (_ck, _v) { var _co = _v.component; var currVal_0 = (_co.stateManagement.app.LoadingIndicator ? "block" : "none"); var currVal_1 = i1.ɵnov(_v, 3).value; var currVal_2 = i1.ɵnov(_v, 3).mode; var currVal_3 = i1.ɵnov(_v, 3)._isNoopAnimation; _ck(_v, 2, 0, currVal_0, currVal_1, currVal_2, currVal_3); }); }
export function View_MainComponent_Host_0(_l) { return i1.ɵvid(0, [(_l()(), i1.ɵeld(0, 0, null, null, 2, "main-app", [], null, null, null, View_MainComponent_0, RenderType_MainComponent)), i1.ɵprd(4608, null, i5.RouterModule, i5.RouterModule, [[2, i5.ɵangular_packages_router_router_a], [2, i5.Router]]), i1.ɵdid(2, 4308992, null, 0, i6.MainComponent, [i7.RoleService, i8.AppStateManagement], null, null)], function (_ck, _v) { _ck(_v, 2, 0); }, null); }
var MainComponentNgFactory = i1.ɵccf("main-app", i6.MainComponent, View_MainComponent_Host_0, {}, {}, []);
export { MainComponentNgFactory as MainComponentNgFactory };
//# sourceMappingURL=MainComponent.ngfactory.js.map