import * as i0 from "@angular/core";
import * as i1 from "./DashboardHeader.ngfactory";
import * as i2 from "./DashboardHeader";
import * as i3 from "../Services/StateManagement/AppStateManagement";
import * as i4 from "./Dashboard";
var styles_Dashboard = [];
var RenderType_Dashboard = i0.ɵcrt({ encapsulation: 2, styles: styles_Dashboard, data: {} });
export { RenderType_Dashboard as RenderType_Dashboard };
export function View_Dashboard_0(_l) { return i0.ɵvid(0, [(_l()(), i0.ɵeld(0, 0, null, null, 4, "div", [["id", "dashboard-container"]], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, ["\n        "])), (_l()(), i0.ɵeld(2, 0, null, null, 1, "tsg-dashboard-header", [], null, null, null, i1.View_DashboardHeader_0, i1.RenderType_DashboardHeader)), i0.ɵdid(3, 114688, null, 0, i2.DashboardHeader, [i3.AppStateManagement], null, null), (_l()(), i0.ɵted(-1, null, ["\n"]))], function (_ck, _v) { _ck(_v, 3, 0); }, null); }
export function View_Dashboard_Host_0(_l) { return i0.ɵvid(0, [(_l()(), i0.ɵeld(0, 0, null, null, 1, "tsg-dashboard", [], null, null, null, View_Dashboard_0, RenderType_Dashboard)), i0.ɵdid(1, 114688, null, 0, i4.Dashboard, [], null, null)], function (_ck, _v) { _ck(_v, 1, 0); }, null); }
var DashboardNgFactory = i0.ɵccf("tsg-dashboard", i4.Dashboard, View_Dashboard_Host_0, {}, {}, []);
export { DashboardNgFactory as DashboardNgFactory };
//# sourceMappingURL=Dashboard.ngfactory.js.map