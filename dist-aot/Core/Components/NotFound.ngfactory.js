import * as i0 from "@angular/core";
import * as i1 from "./NotFound";
var styles_NotFound = [];
var RenderType_NotFound = i0.ɵcrt({ encapsulation: 2, styles: styles_NotFound, data: {} });
export { RenderType_NotFound as RenderType_NotFound };
export function View_NotFound_0(_l) { return i0.ɵvid(0, [(_l()(), i0.ɵeld(0, 0, null, null, 1, "h1", [], null, null, null, null, null)), (_l()(), i0.ɵted(-1, null, [" Not Found"]))], null, null); }
export function View_NotFound_Host_0(_l) { return i0.ɵvid(0, [(_l()(), i0.ɵeld(0, 0, null, null, 1, "ng-component", [], null, null, null, View_NotFound_0, RenderType_NotFound)), i0.ɵdid(1, 114688, null, 0, i1.NotFound, [], null, null)], function (_ck, _v) { _ck(_v, 1, 0); }, null); }
var NotFoundNgFactory = i0.ɵccf("ng-component", i1.NotFound, View_NotFound_Host_0, {}, {}, []);
export { NotFoundNgFactory as NotFoundNgFactory };
//# sourceMappingURL=NotFound.ngfactory.js.map