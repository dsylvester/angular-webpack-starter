var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from "@angular/core";
import "../Html/dashboard.less";
let Dashboard = class Dashboard {
    constructor() {
        this.Init();
    }
    Init() {
    }
    ngOnInit() {
    }
};
Dashboard = __decorate([
    Component({
        selector: "tsg-dashboard",
        template: "<div id=\"dashboard-container\">\r\n        <tsg-dashboard-header></tsg-dashboard-header>\r\n</div>",
        animations: []
    }),
    __metadata("design:paramtypes", [])
], Dashboard);
export { Dashboard };
//# sourceMappingURL=Dashboard.js.map