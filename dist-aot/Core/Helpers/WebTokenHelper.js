var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from "@angular/core";
import { User, Role, UserDetail } from "../Classes/index";
let WebTokenHelper = class WebTokenHelper {
    constructor() {
    }
    hasData(data) {
        if (data) {
            return true;
        }
        else {
            return false;
        }
    }
    mapFromWebToken(token) {
        let parts = token.split('.');
        console.log(parts);
        let obj = atob(parts[1]);
        let tempObj = JSON.parse(obj);
        console.log(`obj: ${JSON.stringify(tempObj, null, 5)}`);
        return this.mapToUser(tempObj);
    }
    mapToUser(data) {
        if (!this.hasData(data)) {
            return null;
        }
        return new User(data.ID, data.FirstName, data.LastName, data.Phone, this.mapToRoles(data.Roles), this.mapToUserDetail(data.AccountDetails));
    }
    mapToRoles(data) {
        if (!this.hasData(data)) {
            return null;
        }
        return data.map(x => {
            return this.mapToRole(x);
        });
    }
    mapToRole(data) {
        if (!this.hasData(data)) {
            return null;
        }
        return new Role(data.ID, data.SecurityLevel, data.Description);
    }
    mapToUserDetail(data) {
        if (!this.hasData(data)) {
            return null;
        }
        return new UserDetail(data.UserID, data.ForcePasswordChange, data.AccountLocked);
    }
};
WebTokenHelper = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [])
], WebTokenHelper);
export { WebTokenHelper };
//# sourceMappingURL=WebTokenHelper.js.map