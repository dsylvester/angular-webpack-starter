var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from "@angular/core";
import { Role } from "../Classes/index";
let RoleHelper = class RoleHelper {
    constructor() {
    }
    hasData(data) {
        if (data !== undefined || data !== null) {
            return true;
        }
        else {
            return false;
        }
    }
    mapToRole(data) {
        if (!this.hasData(data)) {
            return null;
        }
        return new Role(data.ID, data.SecurityLevel, data.Name, data.Description);
    }
    mapToRoles(data) {
        if (!this.hasData(data)) {
            return null;
        }
        return data.map(x => {
            return this.mapToRole(x);
        });
    }
};
RoleHelper = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [])
], RoleHelper);
export { RoleHelper };
//# sourceMappingURL=RoleHelper.js.map