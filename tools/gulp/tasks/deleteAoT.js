let gulp = require("gulp");
let del = require('del');

const lessFile = "./src/ts/**/*.less";


async function deleteAoT() {

    const deletedFiles = await del([
        "./dist-aot"
    ]);

    console.log(`Deleted all files from ${deletedFiles}`)
}


exports.deleteAoT = deleteAoT;
