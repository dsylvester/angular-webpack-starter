let { dest, src, series } = require("gulp");
let fs = require("fs");
let clean = require("./deleteAoT");

const lessFile = "./src/assets/ts/**/*.less";
console.log(lessFile);


console.log(__dirname);


async function copyComponentLessFiles() {

        src([ lessFile ])
        .pipe(dest("./dist-aot"));

    
}


exports.copyComponentLessFiles = series(clean.deleteAoT, copyComponentLessFiles);