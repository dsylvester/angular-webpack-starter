const path = require("path");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const isProduction = process.env.NODE_ENV === "production ";

console.log(`isProduction: ${isProduction}`);

const fs = require("fs");

module.exports = {
    mode: (isProduction) ? "production" : "development",

    context: path.resolve(__dirname),

    entry: {
      polyfills: `${ (isProduction) ? "./dist-aot" : "./src/assets/ts"}/Core/Libs.${ (isProduction) ? "js" : "ts"}`,
      main: `${ (isProduction) ? "./dist-aot" : "./src/assets/ts"}/Core/Main.${ (isProduction) ? "js" : "ts"}`
    },

    output: {
          path: path.resolve(__dirname, `${ (isProduction) ? 'dist' : 'app' }`),
          filename: 'assets/js/[name]-bundle.js'
        },

    optimization: {
        minimize: false,
        splitChunks: {
          chunks: 'all'
          }
    },
    devServer: {
      contentBase: [
        // path.join(__dirname),
        path.join(__dirname, `${ (isProduction) ? 'dist' : 'app' }`),
  
      ],
      compress: true,
      port: 9100,
      open: "",
      host: 'localhost', 
      stats: 'verbose',   
      disableHostCheck: true,
      historyApiFallback: true,
      // watchOptions: {
      //   aggregateTimeout: 300,
      //   poll: 1000
      // }    
    },
    resolve: {
        // Add `.ts` and `.tsx` as a resolvable extension.
        extensions: [".ts", ".tsx", ".js"]
    },
    module: {
        rules: [
          
          { 
            test: /\.ts$/,            
            use: ["ts-loader", "angular2-template-loader" ]
          },
          { test: /\.css$/, loader: 'raw-loader' },
          { test: /\.html$/, loader: 'html-loader' },
          { 
            test: /\.(svg|png|jpg)$/,            
            use: [
              {
                loader: 'url-loader',                
                options: {
                  limit: 1024,
                  emitFile: true,
                  name: '[name].[ext]',
                  path: path.resolve(__dirname, "app"),
                  outputPath: 'assets/images',
                  publicPath: 'assets/images'
                }
              }
            ]
          },
          {
            test: /\.less$/,
            use: [
              //   {
              //     loader: MiniCssExtractPlugin.loader
              //   },
              {
                loader: "style-loader"
              },
              {
                loader: "css-loader"
              },
              {
                loader: "less-loader",
                options: {
                  sourceMap: true
                }
              }
            ],    
          }
        ]
    },
    plugins: [
      // new CopyWebpackPlugin([
      //   { from: '**\\*.less', to: './dist-aot/' },
      //   { logLevel: 'debug' }
      // ]),
      new HtmlWebpackPlugin({
        template: "./src/index.html"
      }),
      new MiniCssExtractPlugin({
        filename: "[name].css",
        chunkFilename: "[id].css"
      })
    ]
  }