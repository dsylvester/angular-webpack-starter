/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		0: 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push([204,2]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ 101:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// CONCATENATED MODULE: ./src/assets/ts/Core/Models/CredentialModel.ts
class CredentialModel {
    constructor(username, password, rememberMe = false) {
        this.username = username;
        this.password = password;
        this.rememberMe = rememberMe;
    }
}

// CONCATENATED MODULE: ./src/assets/ts/Core/Models/UserInfoModel.ts
class UserInfoModel {
    constructor(FullName = "", FirstName = "", LastName = "", EMail = "", FullNameReverse = "") {
        this.FullName = FullName;
        this.FirstName = FirstName;
        this.LastName = LastName;
        this.EMail = EMail;
        this.FullNameReverse = FullNameReverse;
    }
}

// CONCATENATED MODULE: ./src/assets/ts/Core/Models/index.ts
/* concated harmony reexport CredentialModel */__webpack_require__.d(__webpack_exports__, "a", function() { return CredentialModel; });
/* concated harmony reexport UserInfoModel */__webpack_require__.d(__webpack_exports__, "b", function() { return UserInfoModel; });




/***/ }),

/***/ 133:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MainComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(44);
/* harmony import */ var _Services_index__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(31);
/* harmony import */ var _fortawesome_fontawesome_pro_css_all_min_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(185);
/* harmony import */ var _fortawesome_fontawesome_pro_css_all_min_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_fortawesome_fontawesome_pro_css_all_min_css__WEBPACK_IMPORTED_MODULE_3__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





let MainComponent = class MainComponent {
    constructor(roleService, stateManagement) {
        this.roleService = roleService;
        this.stateManagement = stateManagement;
        this.Title = "Main Component Title";
        console.log("Starting Main Component");
    }
    ngOnInit() {
        this.getRolesFromApi();
    }
    ngAfterViewInit() {
    }
    getRolesFromApi() {
        this.roleService.Get().subscribe(x => {
            this.stateManagement.app.Roles = x;
            console.log(this.stateManagement.app.Roles);
        }, x => {
            console.error(x);
        }, () => {
        });
    }
};
MainComponent = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__[/* Component */ "n"])({
        selector: "main-app",
        moduleId: module.i.toString(),
        template: __webpack_require__(186),
        styles: [__webpack_require__(187)],
        providers: [_angular_router__WEBPACK_IMPORTED_MODULE_1__[/* RouterModule */ "c"]]
    }),
    __metadata("design:paramtypes", [_Services_index__WEBPACK_IMPORTED_MODULE_2__[/* RoleService */ "e"],
        _Services_index__WEBPACK_IMPORTED_MODULE_2__[/* AppStateManagement */ "a"]])
], MainComponent);



/***/ }),

/***/ 134:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 134;

/***/ }),

/***/ 186:
/***/ (function(module, exports) {

module.exports = "<div id=\"main-app-container\" class=\"sf-container-stack\">\r\n    <mat-progress-bar [style.display]=\"stateManagement.app.LoadingIndicator ? 'block' : 'none'\" mode=\"indeterminate\">\r\n    </mat-progress-bar>\r\n\r\n    <router-outlet>\r\n\r\n    </router-outlet>\r\n\r\n</div>";

/***/ }),

/***/ 187:
/***/ (function(module, exports) {

module.exports = ":host {\r\n  width: 100vw;\r\n  max-width: 100%;\r\n  min-height: 100vh;\r\n  display: -webkit-box;\r\n  display: -ms-flexbox;\r\n  display: flex;\r\n  -webkit-box-orient: vertical;\r\n  -webkit-box-direction: normal;\r\n      -ms-flex-direction: column;\r\n          flex-direction: column;\r\n  margin: 0;\r\n  -webkit-box-flex: 1;\r\n      -ms-flex-positive: 1;\r\n          flex-grow: 1;\r\n          \r\n}\r\n\r\n/*# sourceMappingURL=styles.css.map */\r\n"

/***/ }),

/***/ 188:
/***/ (function(module, exports) {

module.exports = "<h1> Not Found</h1>";

/***/ }),

/***/ 189:
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(190);

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(77)(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ 190:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(76)(false);
// Module
exports.push([module.i, ".text-center {\n  text-align: center;\n}\n.text-right {\n  text-align: right;\n}\n* {\n  box-sizing: border-box;\n  margin: 0px;\n  padding: 0px;\n}\n:host,\ntsg-login-panel {\n  width: 100vw;\n  max-width: 100%;\n  min-height: 85vh;\n  height: 85vh;\n  display: flex;\n  flex-direction: column;\n  margin: 0;\n  flex-grow: 1;\n  color: #fff;\n  font-family: 'Niramit';\n  box-sizing: border-box;\n}\n.tsg-form-control:last-of-type {\n  margin-bottom: 10px;\n}\np {\n  font-family: 'OPen Sans';\n  text-transform: uppercase;\n  margin: 7px 0px;\n}\np:first-of-type {\n  margin-top: 14px;\n}\np:last-of-type {\n  margin-bottom: 14px;\n}\np:hover {\n  cursor: pointer;\n}\ninput:focus {\n  background: #FFF !important;\n}\nbutton {\n  height: 46px;\n  border-radius: 36px;\n  background: #6334a1;\n  border: 2px solid #211135;\n  color: #fff;\n  font-family: 'OPen Sans';\n  text-transform: uppercase;\n}\nbutton:hover {\n  cursor: pointer;\n  border-color: #ccc;\n}\n.login-background {\n  flex-grow: 1;\n  background-size: 100%;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n}\n.login-container {\n  box-sizing: border-box;\n  display: flex;\n  flex-direction: column;\n  min-width: 250px;\n  width: 35vw;\n  align-items: center;\n}\n.login-section-common {\n  height: 100%;\n  flex-grow: 1;\n  max-width: 48%;\n}\n.login-container-section-wrapper {\n  display: flex;\n  flex-direction: row;\n  flex-grow: 1;\n  height: 100%;\n  width: 100%;\n}\n.container-spacer {\n  border-left: 2px solid #FFF;\n  margin-left: 2%;\n  margin-right: 2%;\n}\n.sign-up {\n  display: flex;\n  flex-direction: column;\n  height: 100%;\n  flex-grow: 1;\n  max-width: 48%;\n}\ninput::placeholder {\n  font-size: 16px;\n  font-family: 'Open Sans';\n  color: #ccc;\n  text-transform: uppercase;\n}\n.new-account {\n  height: 100%;\n  flex-grow: 1;\n  max-width: 48%;\n}\n.formElementGeneral {\n  width: 100%;\n  height: 46px;\n  margin: 2% 0;\n  border: none;\n  padding: 0px 0px 0px 15%;\n  background: #FFF;\n  font-size: 14px;\n  font-family: 'Niramit';\n  border-radius: 7px;\n  font-size: 16px;\n  font-family: 'Open Sans';\n  color: #000;\n}\n.login-container > img {\n  width: 50%;\n  margin-bottom: 10%;\n}\n.login-container-section-wrapper input {\n  box-sizing: border-box;\n  width: 100%;\n  height: 46px;\n  margin: 2% 0;\n  border: none;\n  padding: 0px 0px 0px 15%;\n  background: #FFF;\n  font-size: 14px;\n  font-family: 'Niramit';\n  border-radius: 7px;\n  font-size: 16px;\n  font-family: 'Open Sans';\n  color: #000;\n}\n.tsg-form-control {\n  position: relative;\n}\n.tsg-form-control > i {\n  position: absolute;\n  color: #6334a1;\n  left: 13px;\n  top: 19px;\n  font-size: 22px;\n}\n.btn {\n  width: 100%;\n  height: 46px;\n  margin: 2% 0;\n  border: none;\n  padding: 0px 0px 0px 15%;\n  background: #FFF;\n  font-size: 14px;\n  font-family: 'Niramit';\n  border-radius: 7px;\n  font-size: 16px;\n  font-family: 'Open Sans';\n  color: #000;\n  border: 3px solid #064476;\n  color: #064476;\n  font-size: 1rem;\n  line-height: 46px;\n  text-align: center;\n  text-transform: uppercase;\n  font-weight: 600;\n}\n.btn:hover {\n  cursor: pointer;\n  background: #064476;\n  border-color: #fff;\n  color: #FFF;\n}\n.login-container > div {\n  margin-bottom: 2%;\n}\n.login-container > div > span {\n  color: #000;\n  font-family: 'Niramit';\n  font-size: 0.8rem;\n  text-align: justify;\n}\n.recover-password-container {\n  color: #000;\n}\n.recover-password-container > span:hover {\n  cursor: pointer;\n}\n.mar-bot-2 {\n  margin-bottom: 2%;\n}\n.mar-bot-5 {\n  margin-bottom: 5%;\n}\n.mar-bot-10 {\n  margin-bottom: 10%;\n}\n.gridGeneral {\n  position: relative;\n  display: flex;\n  box-sizing: border-box;\n}\n.sf-expand {\n  flex-grow: 1;\n}\n.sf-container {\n  position: relative;\n  display: flex;\n  box-sizing: border-box;\n}\n.sf-container-stack {\n  position: relative;\n  display: flex;\n  box-sizing: border-box;\n  flex-direction: column;\n}\n.grow {\n  flex-grow: 2;\n}\n.sf-container.right {\n  justify-content: flex-end;\n}\n.sf-container.center {\n  justify-content: center;\n}\n", ""]);



/***/ }),

/***/ 192:
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(193);

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(77)(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ 193:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(76)(false);
// Module
exports.push([module.i, ".snack-davis {\n  left: 40%;\n  max-width: 100%;\n  position: absolute;\n  bottom: 0;\n}\n.snack-bar-container {\n  width: 100vw;\n  max-width: 100%;\n}\nnav #cdk-overlay-2 {\n  width: 100%;\n}\n", ""]);



/***/ }),

/***/ 194:
/***/ (function(module, exports) {

module.exports = "<div class=\"login-background\" simple-snack-bar>\r\n  <div class=\"login-container\">\r\n    <div class=\"login-container-section-wrapper\">\r\n      <div class=\"sign-up\">\r\n        <div class=\"tsg-form-control\">\r\n          <input type=\"text\" placeholder=\"email\" autocomplete=\"off\" [(ngModel)]=\"model.username\" />\r\n          <i class=\"fal fa-user\"></i>\r\n        </div>\r\n\r\n        <div class=\"tsg-form-control\">\r\n          <input type=\"password\" placeholder=\"password\" [(ngModel)]=\"model.password\" />\r\n          <i class=\"far fa-lock-open-alt\"></i>\r\n        </div>\r\n\r\n        <button (click)=\"checkLogin()\">Login</button>\r\n\r\n        <p class=\"text-center\" (click)=\"forgotPassword()\">Forgot Password / Username</p>\r\n        <p class=\"text-center\" (click)=\"resetPassword()\">Reset Password / Username</p>\r\n      </div>\r\n\r\n      <div class=\"container-spacer\"></div>\r\n      <div class=\"new-account\" style=\"padding: 2%;\">\r\n          <p style=\"text-align: left; font-weight: 900;\r\n          font-size: 32px;text-transform: none;font-stretch:300%;\">New <span\r\n            style=\"color: #6334a1;\">User</span></p>\r\n          <p style=\"text-align: left; text-transform: none;font-size: 18px;\">Setup an account</p>\r\n          <p style=\"text-transform: uppercase; font-family: 'Open Sans'; font-size: 225%;\">OR</p>\r\n          <p style=\"text-align: left; text-transform: none;font-size: 18px;\">\r\n            Sign Up today to begin using our state\r\n            of the art system!</p>\r\n\r\n          <button style=\"width: 100%;\">Create Account</button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n";

/***/ }),

/***/ 195:
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(196);

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(77)(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ 196:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(76)(false);
// Module
exports.push([module.i, "tsg-dashboard {\n  width: 100%;\n  display: flex;\n  flex-direction: column;\n}\n", ""]);



/***/ }),

/***/ 197:
/***/ (function(module, exports) {

module.exports = "<div id=\"dashboard-container\">\r\n        <tsg-dashboard-header></tsg-dashboard-header>\r\n</div>";

/***/ }),

/***/ 198:
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(199);

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(77)(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ 199:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(76)(false);
// Module
exports.push([module.i, ".text-center {\n  text-align: center;\n}\n.text-right {\n  text-align: right;\n}\ntsg-dashboard-header {\n  box-sizing: border-box;\n  display: flex;\n  flex-direction: column;\n  width: 100%;\n}\n.iconMargin {\n  display: inline-block;\n  margin-right: 7px;\n  font-size: 18px;\n}\n#dashboard-header-container {\n  display: flex;\n  flex-direction: row;\n  flex-wrap: nowrap;\n  background: #6334a1;\n  border-bottom: 3px solid #211135;\n  height: 55px;\n}\n#dashboard-header-container > ul {\n  box-sizing: border-box;\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  width: 100%;\n  max-width: 100%;\n  height: 55px;\n  overflow: hidden;\n  /* border: 2px dotted yellow; */\n  line-height: 55px;\n  margin: 0px 10px;\n  max-height: 100%;\n}\n#dashboard-header-container > ul > li {\n  box-sizing: border-box;\n  height: 100%;\n  list-style: none;\n  padding: 0px 17px;\n  font-family: 'Open Sans';\n  font-size: 14px;\n  font-weight: 700;\n}\n#dashboard-header-container > ul > li:hover {\n  color: #211135;\n}\n#dashboard-header-container > ul > li > i {\n  display: inline-block;\n  margin-right: 7px;\n  font-size: 18px;\n}\n#dashboard-header-container > div {\n  display: flex;\n  justify-self: flex-end;\n  flex-grow: 1;\n  width: 10%;\n}\n#dashboard-header-container > div > .logged-in-user {\n  justify-self: flex-end;\n  line-height: 55px;\n  font-family: 'Open Sans';\n  font-style: italic;\n  font-size: 16px;\n  font-weight: 700;\n  color: #ac8ad9;\n}\n#dashboard-header-container > div > .logged-in-user:hover {\n  cursor: pointer;\n  color: #fff;\n}\n#dashboard-header-container > div > .logged-in-user > i {\n  display: inline-block;\n  margin-right: 7px;\n  font-size: 18px;\n}\n", ""]);



/***/ }),

/***/ 200:
/***/ (function(module, exports) {

module.exports = "<div id=\"dashboard-header-container\">\r\n        <ul>\r\n                <li><i class=\"fas fa-home\"></i> Home</li>\r\n                <li><i class=\"far fa-cubes\"></i>Projects</li>\r\n        </ul>\r\n\r\n        <div>\r\n                <span class=\"logged-in-user\" *ngIf=\"stateManagement?.app?.UserInfo?.FullName\">\r\n                        <i class=\"fas fa-user-cog\"></i>\r\n                        {{ stateManagement?.app?.UserInfo?.FullName }}\r\n                </span>\r\n        </div>\r\n\r\n        <div>\r\n                <span \r\n                        class=\"logged-in-user\" *ngIf=\"stateManagement?.app?.LoggedIn\"\r\n                        (click)=\"logout()\">\r\n                        <i class=\"fas fa-sign-out-alt\"></i>Sign Off\r\n                </span>\r\n        </div>\r\n\r\n\r\n        \r\n</div>";

/***/ }),

/***/ 201:
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(202);

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(77)(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ 202:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(76)(false);
// Module
exports.push([module.i, ".text-center {\n  text-align: center;\n}\n.text-right {\n  text-align: right;\n}\n:host,\nrecover-password {\n  display: flex;\n  flex-direction: column;\n  flex-basis: 50%;\n  justify-content: center;\n  align-items: center;\n  font-family: 'Niramit';\n  height: 90vh;\n}\n.password-recovery-container {\n  display: flex;\n  flex-direction: column;\n  flex-basis: 30%;\n}\n.tsg-form-control {\n  position: relative;\n  width: 100%;\n  margin-top: 10px;\n  text-align: center;\n}\n.tsg-form-control > i {\n  position: absolute;\n  color: #6334a1;\n  left: 13px;\n  top: 19px;\n  font-size: 22px;\n}\nh3 {\n  font-family: 'Niramit';\n  font-size: 2.2rem;\n  font-weight: 700;\n  color: #6334a1;\n  align-self: center;\n}\nh5 {\n  font-family: 'Niramit';\n  font-size: 1.5rem;\n  font-weight: 600;\n  align-self: center;\n  text-align: center;\n}\ninput[type=\"password\"],\ninput[type=\"email\"] {\n  width: 100%;\n  height: 42px;\n  margin: 0 auto;\n  font-size: 18px;\n  padding: 0 10px;\n  font-family: 'Niramit';\n}\nbutton {\n  height: 46px;\n  border-radius: 36px;\n  background: #6334a1;\n  border: 2px solid #211135;\n  color: #fff;\n  font-family: 'OPen Sans';\n  text-transform: uppercase;\n  padding: 7px 20px;\n}\nbutton:hover {\n  cursor: pointer;\n  border-color: #ccc;\n}\n", ""]);



/***/ }),

/***/ 203:
/***/ (function(module, exports) {

module.exports = "<div class=\"password-recovery-container\">\r\n        <div class=\"recover-username\" *ngIf=\"!showPasswordReset\">\r\n                <h3>We are here to Help</h3>\r\n                <h5>\r\n                        Got it! <br>Please provide your email address that is associated with<br>\r\n                        your account and a link with directions will be sent to you!\r\n                </h5>\r\n                <div class=\"tsg-form-control\">\r\n                        <input type=\"email\" placeholder=\"EMail Address\" [(ngModel)]=\"emailAddress\" />\r\n                        <i class=\"far fa-lock-open-alt\"></i>\r\n                </div>\r\n\r\n                <div class=\"tsg-form-control\">\r\n                        <button id=\"btnSubmit\" (click)=\"verifyEmail(emailAddress)\">Recover Password</button>\r\n                </div>\r\n        </div>\r\n\r\n        <div class=\"reset-password\" *ngIf=\"showPasswordReset\">\r\n\r\n                <h5>\r\n                       Reset Your Password\r\n                </h5>\r\n                \r\n                <div class=\"tsg-form-control\">\r\n                        <input type=\"password\" placeholder=\"Password\" [(ngModel)]=\"passwd\" />\r\n                        <i class=\"far fa-lock-open-alt\"></i>\r\n                </div>\r\n\r\n                <div class=\"tsg-form-control\">\r\n                        <input type=\"password\" placeholder=\"Confirm Password\" [(ngModel)]=\"passwd2\" />\r\n                        <i class=\"far fa-lock-open-alt\"></i>\r\n                </div>\r\n\r\n                <div class=\"tsg-form-control\">\r\n                        <button id=\"btnSubmit\" (click)=\"verifyEmail(emailAddress)\">Save</button>\r\n                </div>\r\n        </div>\r\n</div>";

/***/ }),

/***/ 204:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./node_modules/.registry.npmjs.org/@angular/platform-browser-dynamic/7.2.7/node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js
var platform_browser_dynamic = __webpack_require__(138);

// EXTERNAL MODULE: ./node_modules/.registry.npmjs.org/@angular/core/7.2.7/node_modules/@angular/core/fesm5/core.js
var core = __webpack_require__(0);

// EXTERNAL MODULE: ./node_modules/.registry.npmjs.org/@angular/platform-browser/7.2.7/node_modules/@angular/platform-browser/fesm5/platform-browser.js
var platform_browser = __webpack_require__(19);

// EXTERNAL MODULE: ./node_modules/.registry.npmjs.org/@angular/platform-browser/7.2.7/node_modules/@angular/platform-browser/fesm5/animations.js
var animations = __webpack_require__(25);

// EXTERNAL MODULE: ./node_modules/.registry.npmjs.org/@angular/forms/7.2.7/node_modules/@angular/forms/fesm5/forms.js
var fesm5_forms = __webpack_require__(24);

// EXTERNAL MODULE: ./node_modules/.registry.npmjs.org/@angular/http/7.2.7/node_modules/@angular/http/fesm5/http.js
var http = __webpack_require__(137);

// EXTERNAL MODULE: ./node_modules/.registry.npmjs.org/@angular/common/7.2.7/node_modules/@angular/common/fesm5/http.js
var fesm5_http = __webpack_require__(37);

// EXTERNAL MODULE: ./src/assets/ts/Core/Components/MainComponent.ts
var MainComponent = __webpack_require__(133);

// CONCATENATED MODULE: ./src/assets/ts/Core/Components/NotFound.ts
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

let NotFound = class NotFound {
    constructor() {
    }
    ngOnInit() {
    }
};
NotFound = __decorate([
    Object(core["n" /* Component */])({
        template: __webpack_require__(188),
        styles: [],
    }),
    __metadata("design:paramtypes", [])
], NotFound);


// EXTERNAL MODULE: ./node_modules/.registry.npmjs.org/@angular/router/7.2.7/node_modules/@angular/router/fesm5/router.js + 9 modules
var router = __webpack_require__(44);

// EXTERNAL MODULE: ./src/assets/ts/Core/Services/index.ts + 10 modules
var Services = __webpack_require__(31);

// EXTERNAL MODULE: ./src/assets/ts/Core/Models/index.ts + 2 modules
var Models = __webpack_require__(101);

// EXTERNAL MODULE: ./node_modules/.registry.npmjs.org/@angular/material/7.3.3/node_modules/@angular/material/esm5/snack-bar.es5.js
var snack_bar_es5 = __webpack_require__(210);

// EXTERNAL MODULE: ./src/assets/ts/Core/Helpers/index.ts + 2 modules
var Helpers = __webpack_require__(53);

// EXTERNAL MODULE: ./src/assets/ts/Core/Html/Login.less
var Html_Login = __webpack_require__(189);

// EXTERNAL MODULE: ./src/assets/ts/Core/Html/snack-davis.less
var snack_davis = __webpack_require__(192);

// CONCATENATED MODULE: ./src/assets/ts/Core/Components/Login.ts
var Login_decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var Login_metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








let Login_Login = class Login {
    constructor(authService, router, snackBar, stateManagement, webTokenHelper) {
        this.authService = authService;
        this.router = router;
        this.snackBar = snackBar;
        this.stateManagement = stateManagement;
        this.webTokenHelper = webTokenHelper;
        this.model = {
            username: '',
            password: '',
            rememberMe: false
        };
        this.showLoginPanel = true;
        this.Init();
    }
    Init() {
        console.log(`STarting Login`);
    }
    ngOnInit() {
    }
    checkLogin() {
        if (!this.validateLogin()) {
            return;
        }
        let model = new Models["a" /* CredentialModel */](this.model.username, this.model.password);
        this.stateManagement.app.LoadingIndicator = true;
        this.authService.CheckLogin(model)
            .subscribe(x => {
            console.log(x);
            this.stateManagement.app.Token = x;
            this.stateManagement.authEvents.onLogin();
            console.log(this.stateManagement.app);
        }, y => {
            this.snackBar.open(``, `Incorrect Username / Password combination`, {
                duration: 100000,
                verticalPosition: "top",
                horizontalPosition: "center",
                panelClass: "snack-davis"
            });
            this.model.password = "";
            this.model.username = "";
            console.log(JSON.stringify(y, null, 4));
        }, () => {
            this.stateManagement.app.LoadingIndicator = false;
        });
    }
    forgotPassword() {
        this.router.navigateByUrl(`/recover-password`);
    }
    resetPassword() {
        this.router.navigateByUrl(`/recover-password`);
    }
    navigateToRecoverPassword() {
        console.log(`Navigate to Recover Password`);
        this.toggleShowLoginPanel();
    }
    toggleShowLoginPanel() {
        this.showLoginPanel = (this.showLoginPanel) ? false : true;
    }
    cancel() {
        this.toggleShowLoginPanel();
    }
    validateLogin() {
        const systemAccount = [
            "davis@sylvesterllc.com",
            "admin@TSG.Core.com",
            "system@TSG.Core.com",
            "developers@sylvento.com"
        ];
        const isSystemAccount = systemAccount.some(x => {
            return this.model.username.toLocaleLowerCase() === x.toLocaleLowerCase();
        });
        if (isSystemAccount) {
            return true;
        }
        if (this.model.username.length >= 7 && this.model.password.length >= 5) {
            return true;
        }
        if (this.model.username.length < 7) {
            alert("Your E-mail address must have at least 8 characters");
        }
        if (this.model.password.length < 4) {
            alert("Your Password must have at least 8 characters");
        }
        return false;
    }
};
Login_Login = Login_decorate([
    Object(core["n" /* Component */])({
        selector: "tsg-login-panel",
        template: __webpack_require__(194),
        styles: [],
        animations: []
    }),
    Login_metadata("design:paramtypes", [Services["c" /* AuthenticationService */],
        router["b" /* Router */],
        snack_bar_es5["a" /* MatSnackBar */],
        Services["a" /* AppStateManagement */],
        Helpers["b" /* WebTokenHelper */]])
], Login_Login);


// EXTERNAL MODULE: ./src/assets/ts/Core/Html/dashboard.less
var dashboard = __webpack_require__(195);

// CONCATENATED MODULE: ./src/assets/ts/Core/Components/Dashboard.ts
var Dashboard_decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var Dashboard_metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


let Dashboard = class Dashboard {
    constructor() {
        this.Init();
    }
    Init() {
    }
    ngOnInit() {
    }
};
Dashboard = Dashboard_decorate([
    Object(core["n" /* Component */])({
        selector: "tsg-dashboard",
        template: __webpack_require__(197),
        styles: [],
        animations: []
    }),
    Dashboard_metadata("design:paramtypes", [])
], Dashboard);


// EXTERNAL MODULE: ./src/assets/ts/Core/html/DashboardHeader.less
var DashboardHeader = __webpack_require__(198);

// CONCATENATED MODULE: ./src/assets/ts/Core/Components/DashboardHeader.ts
var DashboardHeader_decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var DashboardHeader_metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



let DashboardHeader_DashboardHeader = class DashboardHeader {
    constructor(stateManagement) {
        this.stateManagement = stateManagement;
        this.Init();
    }
    Init() {
    }
    logout() {
        this.stateManagement.authEvents.onLogout();
    }
    ngOnInit() {
    }
};
DashboardHeader_DashboardHeader = DashboardHeader_decorate([
    Object(core["n" /* Component */])({
        selector: "tsg-dashboard-header",
        template: __webpack_require__(200),
        styles: [],
        animations: []
    }),
    DashboardHeader_metadata("design:paramtypes", [Services["a" /* AppStateManagement */]])
], DashboardHeader_DashboardHeader);


// EXTERNAL MODULE: ./src/assets/ts/Core/Html/PasswordReset.less
var PasswordReset = __webpack_require__(201);

// CONCATENATED MODULE: ./src/assets/ts/Core/Components/PasswordReset.ts
var PasswordReset_decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var PasswordReset_metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




let PasswordReset_PasswordReset = class PasswordReset {
    constructor(route, router, errorService) {
        this.route = route;
        this.router = router;
        this.errorService = errorService;
        this.emailAddress = "";
        this.passwd = "";
        this.passwd2 = "";
        this.showPasswordReset = false;
        this.tid = "";
        this.Init();
    }
    Init() {
    }
    ngOnInit() {
        this.route.queryParams.subscribe(x => {
            if (x.tid) {
                this.tid = x.tid;
                this.showPasswordReset = true;
            }
            console.log(`TID: ${x.tid}`);
        });
    }
    verifyEmail(email) {
        let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        let result = re.test(String(email).toLowerCase());
        if (result) {
            console.log('send to backend');
        }
        else {
            this.errorService.displayError(`Please enter a valid EMail Address`);
        }
    }
};
PasswordReset_PasswordReset = PasswordReset_decorate([
    Object(core["n" /* Component */])({
        selector: "recover-password",
        template: __webpack_require__(203),
        styles: [],
        animations: []
    }),
    PasswordReset_metadata("design:paramtypes", [router["a" /* ActivatedRoute */], router["b" /* Router */],
        Services["d" /* ErrorService */]])
], PasswordReset_PasswordReset);


// CONCATENATED MODULE: ./src/assets/ts/Core/Components/index.ts







// CONCATENATED MODULE: ./src/assets/ts/Core/routes/NavigationRoutes.ts

const NavigationRoutes = [
    { path: "dashboard", component: Dashboard },
    { path: "401", component: NotFound },
    { path: "login", component: Login_Login },
    { path: "recover-password", component: PasswordReset_PasswordReset },
    { path: "", redirectTo: "/login", pathMatch: "full" },
];

// CONCATENATED MODULE: ./src/assets/ts/Core/routes/app.routes.ts


const routes = [];
routes.push(...NavigationRoutes);
const appRoutingProviders = [];
const routing = router["c" /* RouterModule */].forRoot(routes);

// EXTERNAL MODULE: ./node_modules/.registry.npmjs.org/@angular/cdk/7.3.3/node_modules/@angular/cdk/esm5/portal.es5.js
var portal_es5 = __webpack_require__(13);

// EXTERNAL MODULE: ./node_modules/.registry.npmjs.org/@angular/material/7.3.3/node_modules/@angular/material/esm5/button.es5.js
var button_es5 = __webpack_require__(136);

// EXTERNAL MODULE: ./node_modules/.registry.npmjs.org/@angular/material/7.3.3/node_modules/@angular/material/esm5/card.es5.js
var card_es5 = __webpack_require__(211);

// EXTERNAL MODULE: ./node_modules/.registry.npmjs.org/@angular/material/7.3.3/node_modules/@angular/material/esm5/chips.es5.js
var chips_es5 = __webpack_require__(212);

// EXTERNAL MODULE: ./node_modules/.registry.npmjs.org/@angular/material/7.3.3/node_modules/@angular/material/esm5/dialog.es5.js
var dialog_es5 = __webpack_require__(213);

// EXTERNAL MODULE: ./node_modules/.registry.npmjs.org/@angular/material/7.3.3/node_modules/@angular/material/esm5/icon.es5.js
var icon_es5 = __webpack_require__(214);

// EXTERNAL MODULE: ./node_modules/.registry.npmjs.org/@angular/material/7.3.3/node_modules/@angular/material/esm5/input.es5.js + 1 modules
var input_es5 = __webpack_require__(228);

// EXTERNAL MODULE: ./node_modules/.registry.npmjs.org/@angular/material/7.3.3/node_modules/@angular/material/esm5/menu.es5.js + 2 modules
var menu_es5 = __webpack_require__(226);

// EXTERNAL MODULE: ./node_modules/.registry.npmjs.org/@angular/material/7.3.3/node_modules/@angular/material/esm5/progress-bar.es5.js
var progress_bar_es5 = __webpack_require__(215);

// EXTERNAL MODULE: ./node_modules/.registry.npmjs.org/@angular/material/7.3.3/node_modules/@angular/material/esm5/select.es5.js
var select_es5 = __webpack_require__(216);

// EXTERNAL MODULE: ./node_modules/.registry.npmjs.org/@angular/material/7.3.3/node_modules/@angular/material/esm5/toolbar.es5.js
var toolbar_es5 = __webpack_require__(217);

// EXTERNAL MODULE: ./node_modules/.registry.npmjs.org/@angular/material/7.3.3/node_modules/@angular/material/esm5/grid-list.es5.js
var grid_list_es5 = __webpack_require__(218);

// EXTERNAL MODULE: ./node_modules/.registry.npmjs.org/@angular/material/7.3.3/node_modules/@angular/material/esm5/tabs.es5.js
var tabs_es5 = __webpack_require__(219);

// EXTERNAL MODULE: ./node_modules/.registry.npmjs.org/@angular/material/7.3.3/node_modules/@angular/material/esm5/radio.es5.js
var radio_es5 = __webpack_require__(220);

// EXTERNAL MODULE: ./node_modules/.registry.npmjs.org/@angular/material/7.3.3/node_modules/@angular/material/esm5/table.es5.js
var table_es5 = __webpack_require__(221);

// EXTERNAL MODULE: ./node_modules/.registry.npmjs.org/@angular/material/7.3.3/node_modules/@angular/material/esm5/tooltip.es5.js
var tooltip_es5 = __webpack_require__(222);

// EXTERNAL MODULE: ./node_modules/.registry.npmjs.org/@angular/material/7.3.3/node_modules/@angular/material/esm5/checkbox.es5.js
var checkbox_es5 = __webpack_require__(223);

// EXTERNAL MODULE: ./node_modules/.registry.npmjs.org/@angular/material/7.3.3/node_modules/@angular/material/esm5/progress-spinner.es5.js
var progress_spinner_es5 = __webpack_require__(224);

// EXTERNAL MODULE: ./node_modules/.registry.npmjs.org/@angular/cdk/7.3.3/node_modules/@angular/cdk/esm5/table.es5.js
var esm5_table_es5 = __webpack_require__(14);

// CONCATENATED MODULE: ./src/assets/ts/Core/AngularMaterialModule.ts
var AngularMaterialModule_decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




let AngularMaterialModule = class AngularMaterialModule {
};
AngularMaterialModule = AngularMaterialModule_decorate([
    Object(core["L" /* NgModule */])({
        exports: [
            esm5_table_es5["q" /* CdkTableModule */],
            button_es5["a" /* MatButtonModule */],
            card_es5["a" /* MatCardModule */],
            chips_es5["a" /* MatChipsModule */],
            dialog_es5["a" /* MatDialogModule */],
            icon_es5["a" /* MatIconModule */],
            input_es5["a" /* MatInputModule */],
            menu_es5["a" /* MatMenuModule */],
            progress_bar_es5["a" /* MatProgressBarModule */],
            select_es5["a" /* MatSelectModule */],
            toolbar_es5["a" /* MatToolbarModule */],
            grid_list_es5["a" /* MatGridListModule */],
            tabs_es5["a" /* MatTabsModule */],
            radio_es5["a" /* MatRadioModule */],
            table_es5["a" /* MatTableModule */],
            tooltip_es5["a" /* MatTooltipModule */],
            checkbox_es5["a" /* MatCheckboxModule */],
            portal_es5["h" /* PortalModule */],
            progress_spinner_es5["a" /* MatProgressSpinnerModule */],
            progress_bar_es5["a" /* MatProgressBarModule */],
            snack_bar_es5["b" /* MatSnackBarModule */]
        ],
        entryComponents: []
    })
], AngularMaterialModule);


// CONCATENATED MODULE: ./src/assets/ts/Core/app.module.ts
var app_module_decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











let AppModule = class AppModule {
};
AppModule = app_module_decorate([
    Object(core["L" /* NgModule */])({
        imports: [
            platform_browser["a" /* BrowserModule */],
            animations["b" /* BrowserAnimationsModule */],
            fesm5_forms["c" /* FormsModule */],
            fesm5_forms["h" /* ReactiveFormsModule */],
            routing,
            http["a" /* HttpModule */],
            fesm5_http["b" /* HttpClientModule */],
            AngularMaterialModule
        ],
        declarations: [
            MainComponent["a" /* MainComponent */], NotFound, Login_Login, Dashboard, DashboardHeader_DashboardHeader, PasswordReset_PasswordReset
        ],
        entryComponents: [],
        bootstrap: [MainComponent["a" /* MainComponent */]],
        providers: [
            Services["c" /* AuthenticationService */], Services["e" /* RoleService */], Helpers["a" /* RoleHelper */], Services["a" /* AppStateManagement */],
            Helpers["b" /* WebTokenHelper */], Services["b" /* AuthEvents */],
        ]
    })
], AppModule);


// CONCATENATED MODULE: ./src/assets/ts/Core/Main.ts


const platform = Object(platform_browser_dynamic["a" /* platformBrowserDynamic */])();
platform.bootstrapModule(AppModule);


/***/ }),

/***/ 31:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// CONCATENATED MODULE: ./src/assets/ts/Core/Services/StateManagement/Classes/AppGlobal.ts
class AppGlobal {
    constructor() {
        this.loggedIn = false;
        this.UserInfo = null;
        this.Token = null;
        this.Roles = null;
        this.LoadingIndicator = false;
    }
    get LoggedIn() {
        return this.loggedIn;
    }
    set LoggedIn(val) {
        this.loggedIn = val;
    }
}

// CONCATENATED MODULE: ./src/assets/ts/Core/Services/StateManagement/Classes/index.ts


// EXTERNAL MODULE: ./node_modules/.registry.npmjs.org/@angular/core/7.2.7/node_modules/@angular/core/fesm5/core.js
var core = __webpack_require__(0);

// EXTERNAL MODULE: ./node_modules/.registry.npmjs.org/@angular/router/7.2.7/node_modules/@angular/router/fesm5/router.js + 9 modules
var router = __webpack_require__(44);

// EXTERNAL MODULE: ./node_modules/.registry.npmjs.org/rxjs/6.4.0/node_modules/rxjs/_esm5/internal/Subject.js
var Subject = __webpack_require__(10);

// CONCATENATED MODULE: ./src/assets/ts/Core/Services/StateManagement/Reducers/AuthEvents.ts
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


let AuthEvents_AuthEvents = class AuthEvents {
    constructor() {
        this.authEvent = new Subject["a" /* Subject */]();
    }
    onLogout() {
        this.authEvent.next(false);
    }
    onLogin() {
        this.authEvent.next(true);
    }
};
AuthEvents_AuthEvents = __decorate([
    Object(core["D" /* Injectable */])(),
    __metadata("design:paramtypes", [])
], AuthEvents_AuthEvents);


// EXTERNAL MODULE: ./src/assets/ts/Core/Models/index.ts + 2 modules
var Models = __webpack_require__(101);

// EXTERNAL MODULE: ./src/assets/ts/Core/Helpers/index.ts + 2 modules
var Helpers = __webpack_require__(53);

// CONCATENATED MODULE: ./src/assets/ts/Core/Services/StateManagement/AppStateManagement.ts
var AppStateManagement_decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var AppStateManagement_metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






let AppStateManagement_AppStateManagement = class AppStateManagement {
    constructor(router, authEvents, webTokenHelper) {
        this.router = router;
        this.authEvents = authEvents;
        this.webTokenHelper = webTokenHelper;
        this.init();
    }
    init() {
        this.app = new AppGlobal();
        this.subscriptions();
    }
    subscriptions() {
        this.subscribeToAuthEvents();
    }
    subscribeToAuthEvents() {
        this.authSubscription = this.authEvents.authEvent.subscribe(x => {
            if (x) {
                this.setUserAsLoggedIn();
            }
            else {
                this.setUserAsLoggedOut();
            }
        });
    }
    setUserAsLoggedOut() {
        this.app = new AppGlobal();
        this.router.navigateByUrl(`/login`);
    }
    setUserAsLoggedIn() {
        this.app.LoggedIn = true;
        const tokenObj = this.webTokenHelper.mapFromWebToken(this.app.Token);
        this.app.UserInfo = new Models["b" /* UserInfoModel */](`${tokenObj.FirstName} ${tokenObj.LastName}`, tokenObj.FirstName, tokenObj.LastName, "", `${tokenObj.LastName}, ${tokenObj.FirstName} `);
        this.router.navigateByUrl(`/dashboard`);
    }
    unSubscribe() {
        this.authSubscription.unsubscribe();
    }
};
AppStateManagement_AppStateManagement = AppStateManagement_decorate([
    Object(core["D" /* Injectable */])(),
    AppStateManagement_metadata("design:paramtypes", [router["b" /* Router */],
        AuthEvents_AuthEvents,
        Helpers["b" /* WebTokenHelper */]])
], AppStateManagement_AppStateManagement);


// CONCATENATED MODULE: ./src/assets/ts/Core/Services/StateManagement/Reducers/index.ts


// CONCATENATED MODULE: ./src/assets/ts/Core/Services/StateManagement/index.ts




// EXTERNAL MODULE: ./node_modules/.registry.npmjs.org/@angular/common/7.2.7/node_modules/@angular/common/fesm5/http.js
var http = __webpack_require__(37);

// EXTERNAL MODULE: ./node_modules/.registry.npmjs.org/rxjs/6.4.0/node_modules/rxjs/_esm5/internal/operators/map.js
var map = __webpack_require__(21);

// CONCATENATED MODULE: ./src/assets/ts/Core/Services/EndPoints.ts
const Base = "http://localhost:6901";
const EndPoints = {
    Environment: "dev",
    AuthenticationURL: `${Base}/api/authentication`,
    RoleURL: `${Base}/api/secured/role`,
};

// EXTERNAL MODULE: ./src/assets/ts/Core/Classes/index.ts + 5 modules
var Classes = __webpack_require__(55);

// CONCATENATED MODULE: ./src/assets/ts/Core/Services/RoleService.ts
var RoleService_decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var RoleService_metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






let RoleService_RoleService = class RoleService {
    constructor(http, roleHelper) {
        this.http = http;
        this.roleHelper = roleHelper;
        this.Init();
    }
    Init() {
        this.createHeaders();
    }
    createHeaders() {
        this.headers = new http["c" /* HttpHeaders */]({
            "content-type": "application/json",
            "Accept": "application/json",
        });
    }
    Get(id = null) {
        if (id === null) {
            return this.http.get(EndPoints.RoleURL, { headers: this.headers })
                .pipe(Object(map["a" /* map */])(x => {
                return this.roleHelper.mapToRoles(x);
            }));
        }
        else {
            return this.http.get(EndPoints.RoleURL)
                .pipe(Object(map["a" /* map */])(x => {
                return this.roleHelper.mapToRole(x);
            }));
        }
    }
    Post() {
        return new Classes["a" /* Role */]();
    }
    Put() {
        return new Classes["a" /* Role */]();
    }
    Delete() {
    }
};
RoleService_RoleService = RoleService_decorate([
    Object(core["D" /* Injectable */])(),
    RoleService_metadata("design:paramtypes", [http["a" /* HttpClient */],
        Helpers["a" /* RoleHelper */]])
], RoleService_RoleService);


// EXTERNAL MODULE: ./node_modules/.registry.npmjs.org/rxjs/6.4.0/node_modules/rxjs/_esm5/internal/Observable.js + 2 modules
var Observable = __webpack_require__(9);

// EXTERNAL MODULE: ./node_modules/.registry.npmjs.org/rxjs/6.4.0/node_modules/rxjs/_esm5/internal/operators/catchError.js
var catchError = __webpack_require__(206);

// CONCATENATED MODULE: ./src/assets/ts/Core/Services/AuthenticationService.ts
var AuthenticationService_decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var AuthenticationService_metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





let AuthenticationService_AuthenticationService = class AuthenticationService {
    constructor(http) {
        this.http = http;
        this.Init();
    }
    Init() {
        this.createHeaders();
    }
    createHeaders() {
        if (EndPoints.Environment === "dev") {
            this.headers = new http["c" /* HttpHeaders */]({
                "content-type": "application/json",
                "Accept": "application/json",
                "Authorization": "toughsecurity"
            });
        }
        else {
            this.headers = new http["c" /* HttpHeaders */]({
                "content-type": "application/json",
                "Accept": "application/json"
            });
        }
    }
    CheckLogin(data) {
        return this.http.post(EndPoints.AuthenticationURL, data, { headers: this.headers })
            .pipe(Object(map["a" /* map */])(x => {
            return x;
        }))
            .pipe(Object(catchError["a" /* catchError */])(x => {
            console.log(JSON.stringify(x, null, 5));
            return Observable["a" /* Observable */].throw(x);
        }));
    }
};
AuthenticationService_AuthenticationService = AuthenticationService_decorate([
    Object(core["D" /* Injectable */])(),
    AuthenticationService_metadata("design:paramtypes", [http["a" /* HttpClient */]])
], AuthenticationService_AuthenticationService);


// EXTERNAL MODULE: ./node_modules/.registry.npmjs.org/@angular/material/7.3.3/node_modules/@angular/material/esm5/snack-bar.es5.js
var snack_bar_es5 = __webpack_require__(210);

// CONCATENATED MODULE: ./src/assets/ts/Core/Services/ErrorService.ts
var ErrorService_decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var ErrorService_metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


let ErrorService = class ErrorService {
    constructor(snackBar) {
        this.snackBar = snackBar;
    }
    displayError(errorMessage) {
        this.snackBar.open(``, errorMessage, {
            duration: 4000,
            verticalPosition: "top",
            horizontalPosition: "center",
            panelClass: "snack-davis"
        });
    }
};
ErrorService = ErrorService_decorate([
    Object(core["D" /* Injectable */])({
        providedIn: 'root',
    }),
    ErrorService_metadata("design:paramtypes", [snack_bar_es5["a" /* MatSnackBar */]])
], ErrorService);


// CONCATENATED MODULE: ./src/assets/ts/Core/Services/index.ts
/* concated harmony reexport AppStateManagement */__webpack_require__.d(__webpack_exports__, "a", function() { return AppStateManagement_AppStateManagement; });
/* unused concated harmony import AppGlobal */
/* concated harmony reexport AuthEvents */__webpack_require__.d(__webpack_exports__, "b", function() { return AuthEvents_AuthEvents; });
/* concated harmony reexport RoleService */__webpack_require__.d(__webpack_exports__, "e", function() { return RoleService_RoleService; });
/* concated harmony reexport AuthenticationService */__webpack_require__.d(__webpack_exports__, "c", function() { return AuthenticationService_AuthenticationService; });
/* unused concated harmony import EndPoints */
/* concated harmony reexport ErrorService */__webpack_require__.d(__webpack_exports__, "d", function() { return ErrorService; });







/***/ }),

/***/ 53:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./node_modules/.registry.npmjs.org/@angular/core/7.2.7/node_modules/@angular/core/fesm5/core.js
var core = __webpack_require__(0);

// EXTERNAL MODULE: ./src/assets/ts/Core/Classes/index.ts + 5 modules
var Classes = __webpack_require__(55);

// CONCATENATED MODULE: ./src/assets/ts/Core/Helpers/RoleHelper.ts
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


let RoleHelper_RoleHelper = class RoleHelper {
    constructor() {
    }
    hasData(data) {
        if (data !== undefined || data !== null) {
            return true;
        }
        else {
            return false;
        }
    }
    mapToRole(data) {
        if (!this.hasData(data)) {
            return null;
        }
        return new Classes["a" /* Role */](data.ID, data.SecurityLevel, data.Name, data.Description);
    }
    mapToRoles(data) {
        if (!this.hasData(data)) {
            return null;
        }
        return data.map(x => {
            return this.mapToRole(x);
        });
    }
};
RoleHelper_RoleHelper = __decorate([
    Object(core["D" /* Injectable */])(),
    __metadata("design:paramtypes", [])
], RoleHelper_RoleHelper);


// CONCATENATED MODULE: ./src/assets/ts/Core/Helpers/WebTokenHelper.ts
var WebTokenHelper_decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var WebTokenHelper_metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


let WebTokenHelper_WebTokenHelper = class WebTokenHelper {
    constructor() {
    }
    hasData(data) {
        if (data) {
            return true;
        }
        else {
            return false;
        }
    }
    mapFromWebToken(token) {
        let parts = token.split('.');
        console.log(parts);
        let obj = atob(parts[1]);
        let tempObj = JSON.parse(obj);
        console.log(`obj: ${JSON.stringify(tempObj, null, 5)}`);
        return this.mapToUser(tempObj);
    }
    mapToUser(data) {
        if (!this.hasData(data)) {
            return null;
        }
        return new Classes["b" /* User */](data.ID, data.FirstName, data.LastName, data.Phone, this.mapToRoles(data.Roles), this.mapToUserDetail(data.AccountDetails));
    }
    mapToRoles(data) {
        if (!this.hasData(data)) {
            return null;
        }
        return data.map(x => {
            return this.mapToRole(x);
        });
    }
    mapToRole(data) {
        if (!this.hasData(data)) {
            return null;
        }
        return new Classes["a" /* Role */](data.ID, data.SecurityLevel, data.Description);
    }
    mapToUserDetail(data) {
        if (!this.hasData(data)) {
            return null;
        }
        return new Classes["c" /* UserDetail */](data.UserID, data.ForcePasswordChange, data.AccountLocked);
    }
};
WebTokenHelper_WebTokenHelper = WebTokenHelper_decorate([
    Object(core["D" /* Injectable */])(),
    WebTokenHelper_metadata("design:paramtypes", [])
], WebTokenHelper_WebTokenHelper);


// CONCATENATED MODULE: ./src/assets/ts/Core/Helpers/index.ts
/* concated harmony reexport RoleHelper */__webpack_require__.d(__webpack_exports__, "a", function() { return RoleHelper_RoleHelper; });
/* concated harmony reexport WebTokenHelper */__webpack_require__.d(__webpack_exports__, "b", function() { return WebTokenHelper_WebTokenHelper; });




/***/ }),

/***/ 55:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// CONCATENATED MODULE: ./src/assets/ts/Core/Classes/Role.ts
class Role {
    constructor(ID = 0, SecurityLevel = 0, Name = "", Description = "") {
        this.ID = ID;
        this.SecurityLevel = SecurityLevel;
        this.Name = Name;
        this.Description = Description;
    }
}

// CONCATENATED MODULE: ./src/assets/ts/Core/Classes/UserDetail.ts
class UserDetail {
    constructor(UserID = 0, ForcePasswordChange = false, AccountLocked = false) {
        this.UserID = UserID;
        this.ForcePasswordChange = ForcePasswordChange;
        this.AccountLocked = AccountLocked;
    }
}

// CONCATENATED MODULE: ./src/assets/ts/Core/Classes/User.ts

class User_User {
    constructor(ID = 0, FirstName = "", LastName = "", Phone = "", Roles = new Array(), Details = new UserDetail(), FullName = "") {
        this.ID = ID;
        this.FirstName = FirstName;
        this.LastName = LastName;
        this.Phone = Phone;
        this.Roles = Roles;
        this.Details = Details;
        this.FullName = FullName;
        this.FullName = `${this.FirstName} ${this.LastName}`;
    }
}

// EXTERNAL MODULE: ./node_modules/.registry.npmjs.org/@angular/cdk/7.3.3/node_modules/@angular/cdk/esm5/overlay.es5.js
var overlay_es5 = __webpack_require__(20);

// CONCATENATED MODULE: ./src/assets/ts/Core/Classes/custom/CustomOverLay.ts

class CustomOverLay_CustomOverlay extends overlay_es5["d" /* OverlayContainer */] {
    getContainerElement() {
        if (!this._containerElement) {
            this._createContainer();
        }
        return this._containerElement;
    }
    _createContainer() {
        const mainAppName = "main-app";
        const mainAppContainerName = "main-app-container";
        console.log(`This is a test`);
        let mainApp = document.getElementById(mainAppContainerName);
        let container = this._document.createElement('div');
        container.classList.add('cdk-overlay-container');
        mainApp.appendChild(container);
        this._containerElement = container;
    }
}

// CONCATENATED MODULE: ./src/assets/ts/Core/Classes/custom/index.ts


// CONCATENATED MODULE: ./src/assets/ts/Core/Classes/index.ts
/* concated harmony reexport Role */__webpack_require__.d(__webpack_exports__, "a", function() { return Role; });
/* concated harmony reexport User */__webpack_require__.d(__webpack_exports__, "b", function() { return User_User; });
/* concated harmony reexport UserDetail */__webpack_require__.d(__webpack_exports__, "c", function() { return UserDetail; });
/* unused concated harmony import CustomOverlay */






/***/ })

/******/ });