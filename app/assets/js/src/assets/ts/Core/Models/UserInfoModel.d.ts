export declare class UserInfoModel {
    FullName: string;
    FirstName: string;
    LastName: string;
    EMail: string;
    FullNameReverse: string;
    constructor(FullName?: string, FirstName?: string, LastName?: string, EMail?: string, FullNameReverse?: string);
}
