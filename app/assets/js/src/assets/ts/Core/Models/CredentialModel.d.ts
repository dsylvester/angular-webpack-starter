export declare class CredentialModel {
    username: string;
    password: string;
    rememberMe: boolean;
    constructor(username: string, password: string, rememberMe?: boolean);
}
