export declare const EndPoints: {
    Environment: string;
    AuthenticationURL: string;
    RoleURL: string;
};
