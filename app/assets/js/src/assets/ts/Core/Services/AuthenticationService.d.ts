import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { CredentialModel } from "../Models/index";
export declare class AuthenticationService {
    private http;
    private headers;
    constructor(http: HttpClient);
    private Init;
    private createHeaders;
    CheckLogin(data: CredentialModel): Observable<boolean | string>;
}
