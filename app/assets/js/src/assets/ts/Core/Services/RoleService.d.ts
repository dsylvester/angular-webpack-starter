import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { RoleHelper } from "../Helpers/index";
import { Role } from "../Classes/index";
export declare class RoleService {
    private http;
    private roleHelper;
    private headers;
    constructor(http: HttpClient, roleHelper: RoleHelper);
    private Init;
    private createHeaders;
    Get(id?: number | null): Observable<Array<Role | null> | Role | null>;
    Post(): Role;
    Put(): Role;
    Delete(): void;
}
