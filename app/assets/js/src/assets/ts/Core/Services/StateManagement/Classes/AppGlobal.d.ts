import { UserInfoModel } from "../../../Models/index";
import { Role } from "../../../Classes/index";
export declare class AppGlobal {
    constructor();
    private loggedIn;
    LoggedIn: boolean;
    UserInfo: UserInfoModel | null;
    Token: string | null;
    Roles: Array<Role | null> | Role | null;
    LoadingIndicator: boolean;
}
