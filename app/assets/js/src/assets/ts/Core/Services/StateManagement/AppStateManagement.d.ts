import { Router } from "@angular/router";
import { AppGlobal } from "./Classes/index";
import { AuthEvents } from "./Reducers/AuthEvents";
import { WebTokenHelper } from "../../Helpers/index";
export declare class AppStateManagement {
    private router;
    authEvents: AuthEvents;
    webTokenHelper: WebTokenHelper;
    private authSubscription;
    app: AppGlobal;
    constructor(router: Router, authEvents: AuthEvents, webTokenHelper: WebTokenHelper);
    private init;
    private subscriptions;
    private subscribeToAuthEvents;
    private setUserAsLoggedOut;
    setUserAsLoggedIn(): void;
    unSubscribe(): void;
}
