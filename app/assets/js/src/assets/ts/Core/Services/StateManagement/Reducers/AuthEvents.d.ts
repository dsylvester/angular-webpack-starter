import { Subject } from "rxjs";
export declare class AuthEvents {
    authEvent: Subject<boolean>;
    constructor();
    onLogout(): void;
    onLogin(): void;
}
