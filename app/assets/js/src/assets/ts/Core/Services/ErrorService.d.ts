import { MatSnackBar } from '@angular/material';
export declare class ErrorService {
    snackBar: MatSnackBar;
    constructor(snackBar: MatSnackBar);
    displayError(errorMessage: string): void;
}
