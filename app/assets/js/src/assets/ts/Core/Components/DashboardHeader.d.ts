import { OnInit } from "@angular/core";
import { AppStateManagement } from "../Services/index";
import "../html/DashboardHeader.less";
export declare class DashboardHeader implements OnInit {
    stateManagement: AppStateManagement;
    constructor(stateManagement: AppStateManagement);
    private Init;
    logout(): void;
    ngOnInit(): void;
}
