import { OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AuthenticationService, AppStateManagement } from "../Services/index";
import { MatSnackBar } from '@angular/material';
import { WebTokenHelper } from "../Helpers/index";
import "../Html/Login.less";
import "../Html/snack-davis.less";
export declare class Login implements OnInit {
    private authService;
    private router;
    snackBar: MatSnackBar;
    private stateManagement;
    private webTokenHelper;
    model: {
        username: string;
        password: string;
        rememberMe: boolean;
    };
    showLoginPanel: boolean;
    constructor(authService: AuthenticationService, router: Router, snackBar: MatSnackBar, stateManagement: AppStateManagement, webTokenHelper: WebTokenHelper);
    private Init;
    ngOnInit(): void;
    checkLogin(): void;
    forgotPassword(): void;
    resetPassword(): void;
    navigateToRecoverPassword(): void;
    toggleShowLoginPanel(): void;
    cancel(): void;
    validateLogin(): boolean;
}
