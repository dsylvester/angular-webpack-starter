import { OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import "../Html/PasswordReset.less";
import { ErrorService } from "../Services/index";
export declare class PasswordReset implements OnInit {
    private route;
    private router;
    errorService: ErrorService;
    emailAddress: string;
    passwd: string;
    passwd2: string;
    showPasswordReset: boolean;
    tid: string;
    constructor(route: ActivatedRoute, router: Router, errorService: ErrorService);
    private Init;
    ngOnInit(): void;
    verifyEmail(email: string): void;
}
