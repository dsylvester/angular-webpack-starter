import { OnInit, AfterViewInit } from "@angular/core";
import { RoleService, AppStateManagement } from "../Services/index";
import "@angular/material/prebuilt-themes/indigo-pink.css";
import "@fortawesome/fontawesome-pro/css/all.min.css";
export declare class MainComponent implements OnInit, AfterViewInit {
    private roleService;
    stateManagement: AppStateManagement;
    Title: string;
    constructor(roleService: RoleService, stateManagement: AppStateManagement);
    ngOnInit(): void;
    ngAfterViewInit(): void;
    private getRolesFromApi;
}
