export declare class Role {
    ID: number;
    SecurityLevel: number;
    Name: string;
    Description: string;
    constructor(ID?: number, SecurityLevel?: number, Name?: string, Description?: string);
}
