import { OverlayContainer } from "@angular/cdk/overlay";
export declare class CustomOverlay extends OverlayContainer {
    getContainerElement(): HTMLElement;
    protected _createContainer(): void;
}
