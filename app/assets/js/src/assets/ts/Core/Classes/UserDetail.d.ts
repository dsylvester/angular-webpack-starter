export declare class UserDetail {
    UserID: number;
    ForcePasswordChange: boolean;
    AccountLocked: boolean;
    constructor(UserID?: number, ForcePasswordChange?: boolean, AccountLocked?: boolean);
}
