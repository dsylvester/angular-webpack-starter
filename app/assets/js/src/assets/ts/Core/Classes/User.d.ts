import { UserDetail } from "./UserDetail";
import { Role } from "./Role";
export declare class User {
    ID: number;
    FirstName: string;
    LastName: string;
    Phone: string;
    Roles: Array<Role> | null;
    Details: UserDetail;
    FullName: string;
    constructor(ID?: number, FirstName?: string, LastName?: string, Phone?: string, Roles?: Array<Role> | null, Details?: UserDetail, FullName?: string);
}
