import { User, Role } from "../Classes/index";
export declare class WebTokenHelper {
    constructor();
    private hasData;
    mapFromWebToken(token: string): User | null;
    mapToUser(data: any): User | null;
    mapToRoles(data: any): any;
    mapToRole(data: any): Role | null;
    mapToUserDetail(data: any): any;
}
