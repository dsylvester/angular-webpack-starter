import { Role } from "../Classes/index";
export declare class RoleHelper {
    constructor();
    private hasData;
    mapToRole(data: any): Role | null;
    mapToRoles(data: any): Array<Role | null> | null;
}
