const path = require("path");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const isProduction = process.env.NODE_ENV === "production";

console.log(`isProduction: ${isProduction}`);

module.exports = {
  mode: isProduction ? "production" : "development",
  context: path.resolve(__dirname),
  watch: true,

  entry: {
    polyfills: "./src/assets/ts/Core/Libs.ts",
    main: "./src/assets/ts/Core/Main.ts"
  },
  output: {
    path: path.resolve(__dirname, "app"),
    filename: "assets/js/[name]-bundle.js"
    // publicPath: "/"
  },
  optimization: {
    minimize: false,
    splitChunks: {
      chunks: "all"
    }
  },
  devServer: {
    contentBase: [
      // path.join(__dirname),
      path.join(__dirname, "app"),

    ],
    compress: true,
    port: 9000,
    open: true,
    host: 'localhost',
    stats: 'verbose',
    disableHostCheck: true,
    historyApiFallback: true,
    watchOptions: {
      aggregateTimeout: 300,
      poll: 1000
    }
  },
  resolve: {
    // Add `.ts` and `.tsx` as a resolvable extension.
    extensions: [".ts", ".js"]
  },
  module: {
    rules: [{
        test: /\.ts$/,
        use: ["ts-loader", "angular2-template-loader"]
      },
      {
        test: /\.css$/,
        use: ["raw-loader"]
      },
      {
        test: /\.less$/,
        use: [
          //   {
          //     loader: MiniCssExtractPlugin.loader
          //   },
          {
            loader: "style-loader"
          },
          {
            loader: "css-loader"
          },
          {
            loader: "less-loader",
            options: {
              sourceMap: true
            }
          }
        ],

      },
      {
        test: /\.html$/,
        loader: "html-loader"
      },
      {
        test: /\.(svg|png|jpg)$/,
        use: [{
          loader: "file-loader",
          options: {
            limit: 1024,
            emitFile: true,
            name: "[name].[ext]",
            path: path.resolve(__dirname, "app"),
            outputPath: "assets/images",
            publicPath: "assets/images"
          }
        }]
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "./src/index.html"
    }),
    new MiniCssExtractPlugin({
      filename: "[name].css",
      chunkFilename: "[id].css"
    })
  ]
};