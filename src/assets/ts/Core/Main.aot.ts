import { platformBrowser } from "@angular/platform-browser";
// @ts-ignore
import { AppModule } from "./app.module.ngfactory";
import { enableProdMode } from "@angular/core";

enableProdMode();

const platform = platformBrowser();
platform.bootstrapModuleFactory(AppModule);
