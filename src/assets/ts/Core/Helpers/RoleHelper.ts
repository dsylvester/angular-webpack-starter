import { Injectable } from "@angular/core";
import { Role } from "../Classes/index";


@Injectable()
export class RoleHelper {

    constructor() {

    }

    private hasData(data: any): boolean {

        if (data !== undefined || data !== null) {
            return true;
        }
        else {
            return false;
        }
    }

    public mapToRole(data: any): Role|null {
        
        if (!this.hasData(data)) {
            return null;
        }

        return new Role(data.ID,
            data.SecurityLevel,
            data.Name,
            data.Description);
    }

    public mapToRoles(data: any): Array<Role|null>|null {
        
        if (!this.hasData(data)) {
            return null;
        }

        return data.map(x => {
            return this.mapToRole(x!);
        });

    }
}
