import { Injectable } from "@angular/core";
import { User, Role, UserDetail } from "../Classes/index";

@Injectable()
export class WebTokenHelper {

    constructor() {

    }

    private hasData(data: any): boolean {

        // if ((typeof(data) !== 'undefined') || data !== undefined || data !== null) {
        if (data) {
            return true;
        }
        else {
            return false;
        }
    }

    public mapFromWebToken(token: string): User|null {

        let parts = token.split('.');
        console.log(parts);
        let obj = atob(parts[1]);

        let tempObj = JSON.parse(obj);

        console.log(`obj: ${JSON.stringify(tempObj, null, 5)}`);

        return this.mapToUser(tempObj);


    }

    public mapToUser(data: any): User|null {

        if (!this.hasData(data)) {
            return null;
        }
        return new User(data.ID, 
            data.FirstName,
            data.LastName,
            data.Phone,
            this.mapToRoles(data.Roles),
            this.mapToUserDetail(data.AccountDetails));

    } 

    public mapToRoles(data: any): any {

        if (!this.hasData(data)) {
            return null;
        }

        return data.map(x => {
            return this.mapToRole(x!);
        });
    }

    public mapToRole(data: any): Role|null {
        if (!this.hasData(data)) {
            return null;
        }
        return new Role(data.ID,
            data.SecurityLevel,
            data.Description);
    }

    public mapToUserDetail(data: any): any {
        if (!this.hasData(data)) {
            return null;
        }

        return new UserDetail(data.UserID,
            data.ForcePasswordChange,
            data.AccountLocked);
    }
}
