import { NgModule } from "@angular/core";
import { PortalModule } from "@angular/cdk/portal";

import {
    MatButtonModule,
    MatCardModule,
    MatChipsModule,
    MatDialogModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatProgressBarModule,
    MatSelectModule,
    MatToolbarModule,
    MatGridListModule,
    MatTabsModule,
    MatRadioModule,
    MatTableModule,
    MatTooltipModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
    MatSnackBarModule
  } from "@angular/material";
  
  import { CdkTableModule } from '@angular/cdk/table';
  
  @NgModule({
    exports: [
    CdkTableModule, 
    MatButtonModule,
    MatCardModule,
    MatChipsModule,
    MatDialogModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatProgressBarModule,
    MatSelectModule,
    MatToolbarModule,
    MatGridListModule,
    MatTabsModule,
    MatRadioModule,
    MatTableModule,
    MatTooltipModule,
    MatCheckboxModule,
    PortalModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatSnackBarModule
    ], 
    
    entryComponents: []
  })

export class AngularMaterialModule { 


}
