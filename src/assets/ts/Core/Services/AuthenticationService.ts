import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { EndPoints } from "./EndPoints";
import { Observable } from "rxjs";
import { map, catchError } from "rxjs/operators";
import { CredentialModel } from "../Models/index";


@Injectable()
export class AuthenticationService {

    // Properties and Fields
    private headers: HttpHeaders;


    constructor(private http: HttpClient) {
        
        this.Init();
    }

    private Init() {

            this.createHeaders();

    }

    private createHeaders(): void {

        if (EndPoints.Environment === "dev") {
            this.headers = new HttpHeaders({
                "content-type": "application/json",
                "Accept": "application/json",
                "Authorization": "toughsecurity"
            });
        }
        else {
            this.headers = new HttpHeaders({
                "content-type": "application/json",
                "Accept": "application/json"
            });
        }
    }
    
    public CheckLogin(data: CredentialModel): Observable<boolean|string> {

        return this.http.post(EndPoints.AuthenticationURL, data,
            {headers: this.headers})
            .pipe(map(x => {
                return <boolean> x;
            }))
            .pipe(catchError(x => {
                console.log(JSON.stringify(x, null, 5));
                // console.error(x);
                return Observable.throw(x);
            }));

    }  


}
