import { Injectable } from "@angular/core";
import { MatSnackBar } from '@angular/material';


@Injectable({
    providedIn: 'root',
})

export class ErrorService {

   
    constructor(public snackBar: MatSnackBar) {
       
    }
    
    public displayError(errorMessage: string): void {
        this.snackBar.open(``, errorMessage, {
            duration: 4_000,
            verticalPosition: "top",
            horizontalPosition: "center",
            panelClass: "snack-davis"
        });
    }

}
