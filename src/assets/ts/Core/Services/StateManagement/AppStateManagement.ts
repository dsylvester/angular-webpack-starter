import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Subscription } from "rxjs";
import { AppGlobal } from "./Classes/index";
import { AuthEvents } from "./Reducers/AuthEvents";
import { UserInfoModel } from "../../Models/index";
import { User } from "../../Classes";
import { WebTokenHelper } from "../../Helpers/index";

@Injectable()
export class AppStateManagement {

    private authSubscription: Subscription;

    public app: AppGlobal;


    constructor(private router: Router,
        public authEvents: AuthEvents,
        public webTokenHelper: WebTokenHelper) {

        this.init();
    }

    private init(): void {

        this.app = new AppGlobal();
        this.subscriptions();
    }

    private subscriptions(): void {

        this.subscribeToAuthEvents();
    }

    private subscribeToAuthEvents(): void {
        this.authSubscription = this.authEvents.authEvent.subscribe(x => {
            if (x) {
                this.setUserAsLoggedIn();
            }
            else {
                this.setUserAsLoggedOut();
            }
        });
    }

    private setUserAsLoggedOut(): void {
        this.app = new AppGlobal();
        this.router.navigateByUrl(`/login`);
    }

    public setUserAsLoggedIn(): void {
        this.app.LoggedIn = true;

        const tokenObj = <User>this.webTokenHelper.mapFromWebToken(<string>this.app.Token);

        this.app.UserInfo = new UserInfoModel(`${tokenObj.FirstName} ${tokenObj.LastName}`,
            tokenObj.FirstName, tokenObj.LastName, "",
            `${tokenObj.LastName}, ${tokenObj.FirstName} `);

        this.router.navigateByUrl(`/dashboard`);
    }

    public  unSubscribe(): void {
        this.authSubscription.unsubscribe();

    }



}
