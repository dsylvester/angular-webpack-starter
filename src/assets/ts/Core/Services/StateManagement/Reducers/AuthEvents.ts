import { Injectable } from "@angular/core";
import { Subject } from "rxjs";

@Injectable()
export class AuthEvents {

    public authEvent = new Subject<boolean>();

    constructor() {

    }

    public onLogout(): void {
            this.authEvent.next(false);
    }

    public onLogin(): void {
        this.authEvent.next(true);
    }

    
}
