import { UserInfoModel } from "../../../Models/index";
import { Role } from "../../../Classes/index";


export class AppGlobal {

    constructor() {

    }

    private loggedIn = false;
    
    get LoggedIn() {
        return this.loggedIn;
    }

    set LoggedIn(val: boolean) {
        this.loggedIn = val;        
    }

   
    public UserInfo: UserInfoModel | null = null;
    public Token: string | null = null;
    public Roles: Array<Role|null> | Role | null = null;
    public LoadingIndicator = false;


    
}
