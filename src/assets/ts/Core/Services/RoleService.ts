import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { EndPoints } from "./EndPoints";
import { RoleHelper } from "../Helpers/index";
import { Role } from "../Classes/index";

@Injectable()
export class RoleService {

    // Properties and Fields
    private headers: HttpHeaders;


    constructor(private http: HttpClient,
        private roleHelper: RoleHelper) {

        this.Init();
    }

    private Init() {

        this.createHeaders();

    }

    private createHeaders(): void {

        this.headers = new HttpHeaders({
            "content-type": "application/json",
            "Accept": "application/json",
        });
    }

    public Get(id: number | null = null): Observable<Array<Role|null>|Role|null> {

        if (id === null) {
            return this.http.get(EndPoints.RoleURL, {headers: this.headers })
                .pipe(map(x => {
                    return this.roleHelper.mapToRoles(x);
                }));
        }
        else {
            return this.http.get(EndPoints.RoleURL)
                .pipe(map(x => {
                    return this.roleHelper.mapToRole(x);
                }));
        }
    }
   
    public Post(): Role {
        return new Role();
    }

    public Put(): Role {
        return new Role();
    }

    public Delete(): void {

    }




}
