import { OverlayContainer } from "@angular/cdk/overlay";


export class CustomOverlay extends OverlayContainer {


    getContainerElement(): HTMLElement {
        if (!this._containerElement) {
            this._createContainer();
        }
        return this._containerElement;
      }

      protected _createContainer(): void {

        const mainAppName = "main-app";
        const mainAppContainerName = "main-app-container";

        console.log(`This is a test`);

        let mainApp = document.getElementById(mainAppContainerName)!;

        let container = this._document.createElement('div');

        container!.classList.add('cdk-overlay-container');
        mainApp.appendChild(container);
        // this._document.body.appendChild(container);
        this._containerElement = container;
      }
}