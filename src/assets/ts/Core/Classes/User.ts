import { UserDetail } from "./UserDetail";
import { Role } from "./Role";

export class User {

    constructor(public ID: number = 0,
        public FirstName: string = "",
        public LastName: string = "",
        public Phone: string = "",        
        public Roles: Array<Role>|null = new Array<Role>(),
        public Details: UserDetail = new UserDetail(),
        public FullName: string = "") {

            this.FullName = `${this.FirstName} ${this.LastName}`;
    }
}
