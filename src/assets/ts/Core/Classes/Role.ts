export class Role {

    constructor(
        public ID: number = 0,
        public SecurityLevel: number = 0,
        public Name: string = "",
        public Description: string = "") {

    }
}
