export class UserDetail {

    constructor(public UserID: number = 0,
        public ForcePasswordChange: boolean = false,
        public AccountLocked: boolean = false) {

    }
}
