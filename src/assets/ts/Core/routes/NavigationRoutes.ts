import { Routes } from "@angular/router";
import { NotFound, Login, Dashboard, PasswordReset } from "../Components/index";




export const NavigationRoutes: Routes = [
    { path: "dashboard", component: Dashboard },
    { path: "401", component: NotFound },
    { path: "login", component: Login },
    { path: "recover-password", component: PasswordReset },
    { path: "", redirectTo: "/login", pathMatch: "full" },

];


