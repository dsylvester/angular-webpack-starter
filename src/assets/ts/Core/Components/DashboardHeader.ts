import { Component, OnInit } from "@angular/core";
import { AppStateManagement } from "../Services/index";
import "../html/DashboardHeader.less";



@Component({
    selector: "tsg-dashboard-header",
    // moduleId: module.id,
    templateUrl: "../Html/DashboardHeader.html",
    styleUrls: [
     
    ],
    animations: [

    ]
})


export class DashboardHeader implements OnInit  {

    // Properties and Fields
    
    
    constructor(public stateManagement: AppStateManagement) {

        this.Init();

    }

    private Init(): void {

    }

    public logout(): void {
        this.stateManagement.authEvents.onLogout();
    }

    public ngOnInit(): void {
        
    }

    

    
}
