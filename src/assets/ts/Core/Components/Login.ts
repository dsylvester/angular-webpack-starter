import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AuthenticationService, AppStateManagement } from "../Services/index";
import { CredentialModel } from "../Models/index";
import { MatSnackBar } from '@angular/material';
import { WebTokenHelper } from "../Helpers/index";
import "../Html/Login.less";
import "../Html/snack-davis.less";


@Component({
    selector: "tsg-login-panel",
    templateUrl: "../Html/Login.html",
    styleUrls: [

    ],
    animations: [

    ]
})


export class Login implements OnInit {

    // Properties and Fields
    public model = {
        username: '',
        password: '',
        rememberMe: false
    };

    public showLoginPanel = true;

    constructor(private authService: AuthenticationService,
        private router: Router,
        public snackBar: MatSnackBar,
        private stateManagement: AppStateManagement,
        private webTokenHelper: WebTokenHelper) {

        this.Init();

    }

    private Init(): void {
        console.log(`STarting Login`);
    }

    public ngOnInit(): void {

    }

    public checkLogin(): void {

        if (!this.validateLogin()) {
            return;
        }

        let model = new CredentialModel(this.model.username, this.model.password);
        this.stateManagement.app.LoadingIndicator = true;
        this.authService.CheckLogin(model)
            .subscribe(x => {
                console.log(x);

                this.stateManagement.app.Token = <string>x;
                this.stateManagement.authEvents.onLogin();
                console.log(this.stateManagement.app);
            },
                y => {
                    this.snackBar.open(``, `Incorrect Username / Password combination`, {
                        duration: 100000,
                        verticalPosition: "top",
                        horizontalPosition: "center",
                        panelClass: "snack-davis"
                    });
                    this.model.password = "";
                    this.model.username = "";
                    console.log(JSON.stringify(y, null, 4));
                },
                () => {
                    this.stateManagement.app.LoadingIndicator = false;
                });



    }

    public forgotPassword(): void {
        this.router.navigateByUrl(`/recover-password`);
    }

    public resetPassword(): void {
       this.router.navigateByUrl(`/recover-password`);
    }

    public navigateToRecoverPassword(): void {
        console.log(`Navigate to Recover Password`);
        this.toggleShowLoginPanel();

    }

    public toggleShowLoginPanel(): void {
        this.showLoginPanel = (this.showLoginPanel) ? false : true;
    }

    public cancel(): void {
        this.toggleShowLoginPanel();
    }

    public validateLogin(): boolean {
        const systemAccount = [
            "davis@sylvesterllc.com",
            "admin@TSG.Core.com",
            "system@TSG.Core.com",
            "developers@sylvento.com"
        ];


        const isSystemAccount = systemAccount.some(x => {
            return this.model.username.toLocaleLowerCase() === x.toLocaleLowerCase();
        });

        if (isSystemAccount) {
            return true;
        }

        if (this.model.username.length >= 7 && this.model.password.length >= 5) {
            return true;
        }

        if (this.model.username.length < 7) {
            alert("Your E-mail address must have at least 8 characters");
        }


        if (this.model.password.length < 4) {
            alert("Your Password must have at least 8 characters");
        }

        return false;
    }


}
