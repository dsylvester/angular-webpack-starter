import { Component, OnInit, AfterViewInit } from "@angular/core";
import { RouterModule } from "@angular/router";
import { RoleService, AppStateManagement } from "../Services/index";
import "@angular/material/prebuilt-themes/indigo-pink.css";
import "@fortawesome/fontawesome-pro/css/all.min.css";

@Component({
    selector: "main-app",
    moduleId: module.id.toString(),
    templateUrl: "../html/index.html",
    styleUrls: ["../html/styles.css"],
    providers: [RouterModule]
})




export class MainComponent implements OnInit, AfterViewInit {


    public Title = "Main Component Title";



    constructor(private roleService: RoleService,
        public stateManagement: AppStateManagement) {

        console.log("Starting Main Component");

    }

    public ngOnInit(): void {
        this.getRolesFromApi();

    }

    public ngAfterViewInit(): void {

    }

    private getRolesFromApi(): void {

        this.roleService.Get().subscribe(
            x => {
                this.stateManagement.app.Roles = x;
                console.log(this.stateManagement.app.Roles);
            },
            x => {
                console.error(x);

            },
            () => {

            }
        );
    }

}
