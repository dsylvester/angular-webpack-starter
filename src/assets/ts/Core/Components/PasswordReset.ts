import { Component, OnInit } from "@angular/core";
import { MatSnackBar } from '@angular/material';
import { ActivatedRoute, Router } from "@angular/router";
import "../Html/PasswordReset.less";
import { ErrorService } from "../Services/index";

@Component({
    selector: "recover-password",
    templateUrl: "../Html/PasswordReset.html",
    styleUrls: [

    ],
    animations: [

    ]
})


export class PasswordReset implements OnInit  {

    // Properties and Fields
    public emailAddress = "";
    public passwd = "";
    public passwd2 = "";
    public showPasswordReset = false;
    public tid = "";

    constructor(private route: ActivatedRoute, private router: Router,
        public errorService: ErrorService) {

        this.Init();

    }

    private Init(): void {

        

        
    }

    public ngOnInit(): void {

        
        this.route.queryParams.subscribe(x => {
            if (x.tid) {
                this.tid = x.tid;
                this.showPasswordReset = true;                
            }
            
            console.log(`TID: ${x.tid}`);
        });
    }

    public verifyEmail(email: string) {
        // tslint:disable-next-line:max-line-length
        let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        let result = re.test(String(email).toLowerCase());

        if (result) {
            console.log('send to backend');
        }
        else {
            this.errorService.displayError(`Please enter a valid EMail Address`);

            
        }   
     }


}
