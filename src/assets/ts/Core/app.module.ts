import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { HttpClientModule } from "@angular/common/http";
import { MainComponent, NotFound, Login, Dashboard, DashboardHeader,
  PasswordReset }   from "./Components/index";
import { routing } from "./routes/app.routes";
import { AuthenticationService, RoleService,
  AppStateManagement, AuthEvents } from "./Services/index";
import { AngularMaterialModule } from "./AngularMaterialModule";
import { RoleHelper, WebTokenHelper } from "./Helpers/index";
import { CustomOverlay } from "./Classes/index";
import { OverlayContainer } from "@angular/cdk/overlay";




@NgModule({
  imports:      [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    routing,
    HttpModule,
    HttpClientModule,
    AngularMaterialModule
     ],
  declarations: [
                  MainComponent, NotFound, Login, Dashboard, DashboardHeader, PasswordReset
                ],
  entryComponents:
                [

                ],
  bootstrap:    [   MainComponent ],
  providers:    [
                    AuthenticationService, RoleService, RoleHelper, AppStateManagement,
                    WebTokenHelper, AuthEvents,

                   // { provide: OverlayContainer,  useClass: CustomOverlay }
                ]
})


export class AppModule { }
