export class CredentialModel {
    
    constructor(public username: string, 
        public password: string,
        public rememberMe: boolean = false) {

    }
}
