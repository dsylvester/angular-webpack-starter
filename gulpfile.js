const gulp = require("gulp");
let deleteAoTTask = require("./tools/gulp/tasks/deleteAoT");
let lessCopyTask = require("./tools/gulp/tasks/less.copy.aot");

gulp.task('default', () => {
    console.log('default task');
});

gulp.task('deleteAOT', deleteAoTTask.deleteAoT);
gulp.task('lessCopyAOT', lessCopyTask.copyComponentLessFiles);

